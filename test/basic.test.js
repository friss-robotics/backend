const chai = require('chai')
const chaiHttp = require('chai-http')
const server = require('../index')

chai.use(chaiHttp)

describe('Testing random pages', () => {
  it('Test main page - 404', (done) => {
    chai.request(server)
      .get('/')
      .end((err, res) => {
        if (!err) console.error('Error: Testing random pages')
        res.should.have.status(404)
        done()
      })
  })

  it('Test random page - 404', (done) => {
    chai.request(server)
      .get('/fdasjklfasdj342jkl423jkl')
      .end((err, res) => {
        if (!err) console.error('Error: Testing random page - 404')
        res.should.have.status(404)
        done()
      })
  })
})

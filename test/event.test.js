const chai = require('chai')
const chaiHttp = require('chai-http')
const server = require('../index')

chai.use(chaiHttp)

describe('Testing events', () => {
  let user = {
    email: 'test@gmail.com',
    password: 'testtest',
    firstName: 'testf',
    lastName: 'testl',
    handle: 'test',
    educationLvl: 'College',
    Country: 'USA',
    stateOrProv: 'Indiana',
    city: 'West Lafayette',
    school: 'Purdue University'
  }

  let team = {
    name: 'test',
    number: 5,
    info: {
      address: {
        school: 'purdue'
      }
    }
  }

  let event = {
    name: 'test',
    location: 'Indiana',
    eventKey: '2017carv'
  }

  it('Test user registration', (done) => {
    chai.request(server)
      .post('/api/auth/register')
      .set('content-type', 'application/x-www-form-urlencoded')
      .send(user)
      .end((err, res) => {
        if (err) console.error('Error: user registration')
        res.should.have.status(201)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('token')
        res.body.should.have.property('user')
        user._id = res.body.user._id
        user.token = res.body.token
        done()
      })
  })

  it('Creates a team', (done) => {
    chai.request(server)
      .post('/api/team/')
      .set('Authorization', user.token)
      .send(team)
      .end((err, res) => {
        if (err) console.error('Error: team creation')
        res.should.have.status(201)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('team')
        res.body.team.should.have.property('name').that.equals('test')
        res.body.team.should.have.property('info')
        res.body.team.info.should.have.property('address')
        res.body.team.info.address.should.have.property('school').that.equals('purdue')
        team._id = res.body.team._id
        done()
      })
  })

  it('Test event creation', (done) => {
    chai.request(server)
      .post('/api/event/')
      .set('Authorization', user.token)
      .send(event)
      .end((err, res) => {
        if (err) console.error('Error: event creation')
        res.should.have.status(201)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('event')
        res.body.event.should.include(event)
        event._id = res.body.event._id
        done()
      })
  })

  it('Test get event', (done) => {
    chai.request(server)
      .get('/api/event/' + event._id)
      .set('Authorization', user.token)
      .end((err, res) => {
        if (err) console.error('Error: event get')
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('event')
        res.body.event.should.have.property('name').that.equals('test')
        done()
      })
  })

  it('Gets all events', (done) => {
    chai.request(server)
      .get('/api/event')
      .set('Authorization', user.token)
      .end((err, res) => {
        if (err) done(err)
        res.should.have.status(200)
        res.body.should.be.an('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('events').that.is.an('array').that.has.length(1)
        res.body.events[0].should.include(event)
        done()
      })
  })

  it('Test update event', (done) => {
    chai.request(server)
      .post('/api/event/' + event._id)
      .set('Authorization', user.token)
      .send({
        name: 'testest'
      })
      .end((err, res) => {
        if (err) console.error('Error: event update')
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('event')
        res.body.event.should.have.property('name').that.equals('testest')
        done()
      })
  })
  it('Test delete event', (done) => {
    chai.request(server)
      .delete('/api/event/' + event._id)
      .set('Authorization', user.token)
      .end((err, res) => {
        if (err) console.error('Error: event delete')
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        done()
      })
  })

  it('Deletes team', (done) => {
    chai.request(server)
      .delete('/api/team/')
      .set('Authorization', user.token)
      .end((err, res) => {
        if (err) console.error('Error: team delete')
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        done()
      })
  })

  it('Test user deletion', (done) => {
    chai.request(server)
      .delete('/api/account')
      .set('content-type', 'application/x-www-form-urlencoded')
      .set('Authorization', user.token)
      .send({
        password: user.password
      })
      .end((err, res) => {
        if (err) console.error('Error: user delete')
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        done()
      })
  })
})

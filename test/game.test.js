const chai = require('chai')
const chaiHttp = require('chai-http')
const server = require('../index')

chai.use(chaiHttp)

describe('Game', () => {
  let user = {
    email: 'test@gmail.com',
    password: 'testtest',
    firstName: 'testf',
    lastName: 'testl',
    handle: 'test',
    educationLvl: 'College',
    Country: 'USA',
    stateOrProv: 'Indiana',
    city: 'West Lafayette',
    school: 'Purdue University'
  }

  let game = {
    name: 'gameTest',
    description: 'This is a test'
  }

  let game2 = {
    name: 'gameTest 2',
    description: 'This is a second test'
  }

  let metric = {
    name: 'test',
    section: 'Autonomous Mode',
    description: 'testest',
    type: 'Integer',
    defaultValue: '5',
    maximumValue: 10,
    minimumValue: 1,
    incrementStep: 1
  }

  let team = {
    name: 'test',
    number: 5,
    info: {
      address: {
        school: 'purdue'
      }
    }
  }

  it('Registers user', (done) => {
    chai.request(server)
      .post('/api/auth/register')
      .set('content-type', 'application/x-www-form-urlencoded')
      .send(user)
      .end((err, res) => {
        if (err) console.error('Error: registration')
        res.should.have.status(201)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('token')
        res.body.should.have.property('user')
        user._id = res.body.user._id
        user.token = res.body.token
        done()
      })
  })

  it('Creates a team', (done) => {
    chai.request(server)
      .post('/api/team/')
      .set('Authorization', user.token)
      .send(team)
      .end((err, res) => {
        if (err) console.error('Error: team creation')
        res.should.have.status(201)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('team')
        res.body.team.should.have.property('name').that.equals('test')
        res.body.team.should.have.property('info')
        res.body.team.info.should.have.property('address')
        res.body.team.info.address.should.have.property('school').that.equals('purdue')
        team._id = res.body.team._id
        done()
      })
  })

  it('Creates a game successfully', (done) => {
    chai.request(server)
      .post('/api/games/')
      .set('Authorization', user.token)
      .send(game)
      .end((err, res) => {
        if (err) console.error('Error: game creation')
        res.should.have.status(201)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('game')
        res.body.game.should.have.property('name').that.equals('gameTest')
        res.body.game.should.have.property('description').that.equals('This is a test')
        game._id = res.body.game._id
        done()
      })
  })

  it('Retrieves the game', (done) => {
    chai.request(server)
      .get('/api/games/' + game._id)
      .set('Authorization', user.token)
      .end((err, res) => {
        if (err) console.error('Error: game get')
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('game')
        res.body.game.should.have.property('name').that.equals('gameTest')
        res.body.game.should.have.property('description').that.equals('This is a test')
        done()
      })
  })

  it('Retrieves all games', (done) => {
    chai.request(server)
      .get('/api/games')
      .set('Authorization', user.token)
      .end((err, res) => {
        if (err) console.error('Error: game get')
        res.should.have.status(200)
        res.body.should.be.an('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('games').that.is.an('array').that.has.lengthOf(1)
        res.body.games[0].should.include(game)
        done()
      })
  })

  it('Creates a second game', (done) => {
    chai.request(server)
      .post('/api/games/')
      .set('Authorization', user.token)
      .send(game2)
      .end((err, res) => {
        if (err) console.error('Error: game creation')
        res.should.have.status(201)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('game')
        res.body.game.should.include(game2)
        game2._id = res.body.game._id
        done()
      })
  })

  it('Retrieves both games', (done) => {
    chai.request(server)
      .get('/api/games')
      .set('Authorization', user.token)
      .end((err, res) => {
        if (err) console.error('Error: game get')
        res.should.have.status(200)
        res.body.should.be.an('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('games').that.is.an('array').that.has.lengthOf(2)
        res.body.games[0].should.include(game)
        res.body.games[1].should.include(game2)
        done()
      })
  })

  it('Updates a game', (done) => {
    chai.request(server)
      .post('/api/games/' + game._id)
      .set('Authorization', user.token)
      .send({
        name: 'gameTestUpdate',
        description: 'This is a bigger test'
      })
      .end((err, res) => {
        if (err) console.error('Error: game update')
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('game')
        res.body.game.should.have.property('name').that.equals('gameTestUpdate')
        res.body.game.should.have.property('description').that.equals('This is a bigger test')
        done()
      })
  })

  it('Adds a metric to a game', (done) => {
    chai.request(server)
      .post('/api/games/' + game._id + '/metrics')
      .set('Authorization', user.token)
      .send(metric)
      .end((err, res) => {
        if (err) console.error('Error: metric creation')
        res.should.have.status(200)
        res.body.should.be.an('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('metrics')
        res.body.should.have.property('metrics').that.is.an('array')
        res.body.should.have.property('metrics').that.has.lengthOf(1)
        res.body.metrics[0].should.include(metric)
        metric._id = res.body.metrics[0]._id
        done()
      })
  })

  it('Updates a metric in a game', (done) => {
    chai.request(server)
      .put('/api/games/' + game._id + '/metrics/' + metric._id)
      .set('Authorization', user.token)
      .send({
        type: 'Integer',
        maximumValue: 9,
        minimumValue: 2
      })
      .end((err, res) => {
        if (err) console.error('Error: metric update')
        res.should.have.status(200)
        res.body.should.be.an('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('metric')
        res.body.metric.should.have.property('maximumValue').that.equals(9)
        res.body.metric.should.have.property('minimumValue').that.equals(2)
        done()
      })
  })

  it('Deletes a metric successfully', (done) => {
    chai.request(server)
      .delete(`/api/games/${game._id}/metrics/${metric._id}`)
      .set('Authorization', user.token)
      .end((err, res) => {
        if (err) done(err)
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        done()
      })
  })

  it('Retrieves game successfully', (done) => {
    chai.request(server)
      .get('/api/games/' + game._id)
      .set('Authorization', user.token)
      .end((err, res) => {
        if (err) console.error('Error: game get')
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('game')
        res.body.game.should.have.property('name').that.equals('gameTestUpdate')
        res.body.game.should.have.property('description').that.equals('This is a bigger test')
        done()
      })
  })

  it('Deletes game', (done) => {
    chai.request(server)
      .delete('/api/games/' + game._id)
      .set('Authorization', user.token)
      .end((err, res) => {
        if (err) console.error('Error: game deletion')
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        done()
      })
  })

  it('Deletes second game', (done) => {
    chai.request(server)
      .delete('/api/games/' + game2._id)
      .set('Authorization', user.token)
      .end((err, res) => {
        if (err) console.error('Error: game deletion')
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        done()
      })
  })

  it('Deletes team', (done) => {
    chai.request(server)
      .delete('/api/team/')
      .set('Authorization', user.token)
      .end((err, res) => {
        if (err) console.error('Error: team delete')
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        done()
      })
  })

  it('Deletes user', (done) => {
    chai.request(server)
      .delete('/api/account')
      .set('Authorization', user.token)
      .send({
        password: user.password
      })
      .end((err, res) => {
        if (err) console.error('Error: user delete')
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        done()
      })
  })
})

const chai = require('chai')
const chaiHttp = require('chai-http')
const server = require('../index')

chai.use(chaiHttp)

describe('Testing reports', () => {
  let user = {
    email: 'test@gmail.com',
    password: 'testtest',
    firstName: 'testf',
    lastName: 'testl',
    handle: 'test',
    educationLvl: 'College',
    Country: 'USA',
    stateOrProv: 'Indiana',
    city: 'West Lafayette',
    school: 'Purdue University',
    canScout: true,
    canAnalyze: true
  }

  let user2 = {
    email: 'test2@gmail.com',
    password: 'testtesttest',
    firstName: 'testf2',
    lastName: 'testl2',
    handle: 'test2',
    educationLvl: 'College',
    Country: 'USA',
    stateOrProv: 'Indiana',
    city: 'West Lafayette',
    school: 'Purdue University',
    canScout: false,
    canAnalyze: false
  }

  let team = {
    name: 'test',
    number: 5
  }

  let event = {
    name: 'test',
    location: 'Indiana',
    eventKey: '2017carv'
  }

  let scoutingReport = {
    matchID: 'qa1',
    matchNumber: 1,
    robotPos: 'red1',
    teamKey: '123'
  }

  let allianceReport = {
    name: 'test'
  }

  let scoutingReport1 = {
    matchID: '49dbab231971f74d21ba1758',
    robotPos: 'blue1',
    teamKey: '33'
  }

  let scoutingReport2 = {
    matchID: '49dbab231971f74d21ba1111',
    robotPos: 'blue2',
    teamKey: '33'
  }

  let game = {
    name: 'gameTest',
    description: 'This is a test'
  }

  let metric = {
    name: 'metric0',
    section: 'Autonomous Mode',
    description: 'testest',
    type: 'Integer',
    defaultValue: '5',
    maximumValue: 10,
    minimumValue: 1,
    incrementStep: 1
  }

  let metric1 = {
    name: 'metric1',
    section: 'Autonomous Mode',
    description: 'testest',
    type: 'Integer',
    defaultValue: '1',
    maximumValue: 15,
    minimumValue: 0,
    incrementStep: 2
  }

  it('Test user registration', (done) => {
    chai.request(server)
      .post('/api/auth/register')
      .set('content-type', 'application/x-www-form-urlencoded')
      .send(user)
      .end((err, res) => {
        if (err) console.error('Error: user registration')
        res.should.have.status(201)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('token')
        res.body.should.have.property('user')
        user._id = res.body.user._id
        user.token = res.body.token
        done()
      })
  })

  it('Test team creation', (done) => {
    chai.request(server)
      .post('/api/team/')
      .set('content-type', 'application/x-www-form-urlencoded')
      .set('Authorization', user.token)
      .send(team)
      .end((err, res) => {
        if (err) console.error('Error: team creation')
        res.should.have.status(201)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('team')
        res.body.team.should.have.property('name').that.equals('test')
        team._id = res.body.team._id
        done()
      })
  })

  it('Creates a game successfully', (done) => {
    chai.request(server)
      .post('/api/games/')
      .set('Authorization', user.token)
      .send(game)
      .end((err, res) => {
        if (err) console.error('Error: game creation')
        res.should.have.status(201)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('game')
        res.body.game.should.have.property('name').that.equals('gameTest')
        res.body.game.should.have.property('description').that.equals('This is a test')
        game._id = res.body.game._id
        done()
      })
  })

  it('Adds a metric to a game', (done) => {
    chai.request(server)
      .post('/api/games/' + game._id + '/metrics')
      .set('Authorization', user.token)
      .send(metric)
      .end((err, res) => {
        if (err) console.error('Error: metric creation')
        res.should.have.status(200)
        res.body.should.be.an('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('metrics')
        res.body.should.have.property('metrics').that.is.an('array')
        res.body.should.have.property('metrics').that.has.lengthOf(1)
        res.body.metrics[0].should.include(metric)
        metric._id = res.body.metrics[0]._id
        done()
      })
  })

  it('Adds a metric to a game', (done) => {
    chai.request(server)
      .post('/api/games/' + game._id + '/metrics')
      .set('Authorization', user.token)
      .send(metric1)
      .end((err, res) => {
        if (err) console.error('Error: metric creation')
        res.should.have.status(200)
        res.body.should.be.an('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('metrics')
        res.body.should.have.property('metrics').that.is.an('array')
        res.body.should.have.property('metrics').that.has.lengthOf(2)
        res.body.metrics[1].should.include(metric1)
        metric1._id = res.body.metrics[1]._id
        done()
      })
  })

  it('Test event creation', (done) => {
    chai.request(server)
      .post('/api/event/')
      .set('Authorization', user.token)
      .send(event)
      .end((err, res) => {
        if (err) console.error('Error: event creation')
        res.should.have.status(201)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('event')
        // res.body.event.should.include(event)
        event._id = res.body.event._id
        done()
      })
  })

  it('Test update event', (done) => {
    chai.request(server)
      .post('/api/event/' + event._id)
      .set('Authorization', user.token)
      .send({
        game: game._id
      })
      .end((err, res) => {
        if (err) console.error('Error: event update')
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('event')
        done()
      })
  })

  it('Test scouting report creation', (done) => {
    chai.request(server)
      .post('/api/report/scouting/')
      // .set('content-type', 'application/x-www-form-urlencoded')
      .set('Authorization', user.token)
      .send({
        matchID: '49dbab231971f74d21ba1758',
        eventID: event._id,
        robotPos: 'blue1',
        teamKey: '33',
        metricData: [{
          metric: metric._id,
          metricValue: '10'
        }, {
          metric: metric1._id,
          metricValue: '15'
        }]
      })
      .end((err, res) => {
        if (err) console.error('Error: scouting report create')
        res.should.have.status(201)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('scoutingReport')
        scoutingReport1._id = res.body.scoutingReport._id
        scoutingReport1.robotPos = res.body.scoutingReport.robotPos
        scoutingReport1.teamKey = res.body.scoutingReport.teamKey
        done()
      })
  })

  it('Test scouting report creation', (done) => {
    chai.request(server)
      .post('/api/report/scouting/')
      // .set('content-type', 'application/x-www-form-urlencoded')
      .set('Authorization', user.token)
      .send({
        matchID: '49dbab231971f74d21ba1111',
        eventID: event._id,
        robotPos: 'blue2',
        teamKey: '33',
        metricData: [{
          metric: metric1._id,
          metricValue: '3'
        }, {
          metric: metric._id,
          metricValue: '2'
        }]
      })
      .end((err, res) => {
        if (err) console.error('Error: scouting report create')
        res.should.have.status(201)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('scoutingReport')
        scoutingReport2._id = res.body.scoutingReport._id
        scoutingReport2.robotPos = res.body.scoutingReport.robotPos
        scoutingReport2.teamKey = res.body.scoutingReport.teamKey
        done()
      })
  })

  it('Test alliance report creation', (done) => {
    chai.request(server)
      .post('/api/report/alliance/')
      // .set('content-type', 'application/x-www-form-urlencoded')
      .set('Authorization', user.token)
      .send({name: 'test',
        teamID: team._id,
        eventID: event._id,
        weights: [{
          metric: metric._id,
          weight: 1
        }, {
          metric: metric1._id,
          weight: 1
        }]
      })
      .end((err, res) => {
        if (err) console.error('Error: alliance report creation')
        res.should.have.status(201)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('allianceReport')
        res.body.allianceReport.should.have.property('name').that.equals('test')
        allianceReport._id = res.body.allianceReport._id
        res.body.allianceReport.should.have.property('results')
        res.body.allianceReport.results[0].should.have.property('teamNum')
        res.body.allianceReport.results[0].should.have.property('score')
        // console.log(res.body.allianceReport.results)
        done()
      })
  })

  it('Test delete scouting report', (done) => {
    chai.request(server)
      .delete('/api/report/scouting/' + scoutingReport1._id)
      .set('Authorization', user.token)
      .end((err, res) => {
        if (err) console.error('Error: scouting report delete')
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        done()
      })
  })

  it('Test delete scouting report', (done) => {
    chai.request(server)
      .delete('/api/report/scouting/' + scoutingReport2._id)
      .set('Authorization', user.token)
      .end((err, res) => {
        if (err) console.error('Error: scouting report delete')
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        done()
      })
  })

  it('Test get alliance report', (done) => {
    chai.request(server)
      .get('/api/report/alliance/' + allianceReport._id)
      .set('Authorization', user.token)
      .end((err, res) => {
        if (err) console.error('Error: alliance report get')
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('allianceReport')
        res.body.allianceReport.should.have.property('name').that.equals('test')
        done()
      })
  })

  it('Test get alliance reports', (done) => {
    chai.request(server)
      .get('/api/report/alliance')
      .set('Authorization', user.token)
      .end((err, res) => {
        if (err) console.error('Error: get alliance reports')
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('reports').that.is.an('array').that.has.lengthOf(1)
        res.body.reports[0].should.have.property('name').that.equals('test')
        done()
      })
  })

  it('Test update alliance report', (done) => {
    chai.request(server)
      .post('/api/report/alliance/' + allianceReport._id)
      .set('Authorization', user.token)
      .send({
        name: 'testest'
      })
      .end((err, res) => {
        if (err) console.error('Error: alliance report update')
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('allianceReport')
        res.body.allianceReport.should.have.property('name').that.equals('testest')
        done()
      })
  })

  it('Test create alliance report weight', (done) => {
    chai.request(server)
        .post('/api/report/alliance/' + allianceReport._id)
        .set('Authorization', user.token)
        .send({
          weight: {
            metric: user._id, // Yes, this should be a metric ID, but that doesn't matter for this test
            weight: 5
          }
        })
        .end((err, res) => {
          if (err) console.error('Error: alliance report update')
          res.should.have.status(200)
          res.body.should.be.a('object')
          res.body.should.have.property('success').that.equals(true)
          res.body.should.have.property('allianceReport')
          res.body.allianceReport.should.have.property('weights')
          done()
        })
  })

  it('Test create alliance report weight 2', (done) => {
    chai.request(server)
        .post('/api/report/alliance/' + allianceReport._id)
        .set('Authorization', user.token)
        .send({
          weight: {
            metric: team._id, // Yes, this should be a metric ID, but that doesn't matter for this test
            weight: 8
          }
        })
        .end((err, res) => {
          if (err) console.error('Error: alliance report update')
          res.should.have.status(200)
          res.body.should.be.a('object')
          res.body.should.have.property('success').that.equals(true)
          res.body.should.have.property('allianceReport')
          res.body.allianceReport.should.have.property('weights')
          done()
        })
  })

  it('Test update alliance report weight', (done) => {
    chai.request(server)
        .post('/api/report/alliance/weight/' + allianceReport._id)
        .set('Authorization', user.token)
        .send({
          metric: user._id, // Yes, this should be a metric ID, but that doesn't matter for this test
          weight: 10
        })
        .end((err, res) => {
          if (err) console.error('Error: alliance report update')
          res.should.have.status(200)
          res.body.should.be.a('object')
          res.body.should.have.property('success').that.equals(true)
          res.body.should.have.property('allianceReport')
          res.body.allianceReport.should.have.property('weights')
          done()
        })
  })

  it('Test update alliance report weight 2', (done) => {
    chai.request(server)
        .post('/api/report/alliance/weight/' + allianceReport._id)
        .set('Authorization', user.token)
        .send({
          metric: team._id, // Yes, this should be a metric ID, but that doesn't matter for this test
          weight: 12
        })
        .end((err, res) => {
          if (err) console.error('Error: alliance report update')
          res.should.have.status(200)
          res.body.should.be.a('object')
          res.body.should.have.property('success').that.equals(true)
          res.body.should.have.property('allianceReport')
          res.body.allianceReport.should.have.property('weights')
          done()
        })
  })

  it('Test delete alliance report', (done) => {
    chai.request(server)
      .delete('/api/report/alliance/' + allianceReport._id)
      .set('Authorization', user.token)
      .end((err, res) => {
        if (err) console.error('Error: alliance report delete')
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        done()
      })
  })

  it('Test scouting report creation', (done) => {
    chai.request(server)
      .post('/api/report/scouting/')
      .set('content-type', 'application/x-www-form-urlencoded')
      .set('Authorization', user.token)
      .send(scoutingReport)
      .end((err, res) => {
        if (err) console.error('Error: scouting report create')
        res.should.have.status(201)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('scoutingReport')
        res.body.scoutingReport.should.include(scoutingReport)
        scoutingReport._id = res.body.scoutingReport._id
        scoutingReport.robotPos = res.body.scoutingReport.robotPos
        scoutingReport.scoutTeam = res.body.scoutingReport.scoutTeam
        done()
      })
  })

  it('Test duplicate scouting report creation', (done) => {
    chai.request(server)
      .post('/api/report/scouting/')
      .set('content-type', 'application/x-www-form-urlencoded')
      .set('Authorization', user.token)
      .send(scoutingReport)
      .end((err, res) => {
        if (!err) console.error('Error: Test duplicate scouting report creation')
        res.should.have.status(400)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(false)
        res.body.should.have.property('error').that.equals('Scouting Report already exists!')
        done()
      })
  })

  it('Test user2 registration', (done) => {
    chai.request(server)
      .post('/api/auth/register')
      .set('content-type', 'application/x-www-form-urlencoded')
      .send(user2)
      .end((err, res) => {
        if (err) console.error('Error: user registration')
        res.should.have.status(201)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('token')
        res.body.should.have.property('user')
        user2._id = res.body.user._id
        user2.token = res.body.token
        done()
      })
  })

  it('Test scouting report creation without can scout permission', (done) => {
    chai.request(server)
      .post('/api/report/scouting/')
      .set('content-type', 'application/x-www-form-urlencoded')
      .set('Authorization', user2.token)
      .send(scoutingReport)
      .end((err, res) => {
        if (!err) console.error('Error: Test scouting report creation without can scout permission')
        res.should.have.status(401)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(false)
        res.body.should.have.property('error').that.equals('You do not have permission to do that')
        done()
      })
  })

  it('Test alliance report creation without can analyze permission', (done) => {
    chai.request(server)
        .post('/api/report/alliance/')
        .set('content-type', 'application/x-www-form-urlencoded')
        .set('Authorization', user2.token)
        .send(scoutingReport)
        .end((err, res) => {
          if (!err) console.error('Error: Test alliance report creation without can analyze permission')
          res.should.have.status(401)
          res.body.should.be.a('object')
          res.body.should.have.property('success').that.equals(false)
          res.body.should.have.property('error').that.equals('You do not have the permission to create this!')
          done()
        })
  })

  it('Test get alliance report without permission', (done) => {
    chai.request(server)
        .get('/api/report/alliance/' + allianceReport._id)
        .set('Authorization', user2.token)
        .end((err, res) => {
          if (!err) console.error('Error: Test get alliance report without permission')
          res.should.have.status(401)
          res.body.should.be.a('object')
          res.body.should.have.property('success').that.equals(false)
          res.body.should.have.property('error').that.equals('You do not have the permission to view this!')
          done()
        })
  })

  it('Test get scouting report', (done) => {
    chai.request(server)
      .get('/api/report/scouting/' + scoutingReport._id)
      .set('Authorization', user.token)
      .end((err, res) => {
        if (err) console.error('Error: scouting report get')
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('scoutingReport')
        res.body.scoutingReport.should.have.property('robotPos').that.equals('red1')
        res.body.scoutingReport.should.have.property('teamKey').that.equals('123')
        done()
      })
  })

  it('Test get scouting report without can scout permission', (done) => {
    chai.request(server)
      .get('/api/report/scouting/' + scoutingReport._id)
      .set('Authorization', user2.token)
      .end((err, res) => {
        if (!err) console.error('Error: Test get scouting report without can scout permission')
        res.should.have.status(401)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(false)
        res.body.should.have.property('error').that.equals('You do not have permission to view this!')
        done()
      })
  })

  it('Test update scouting report', (done) => {
    chai.request(server)
      .post('/api/report/scouting/' + scoutingReport._id)
      .set('Authorization', user.token)
      .send({
        matchID: '59dbab61e971f74d21b48759',
        robotPos: 'blue1'
      })
      .end((err, res) => {
        if (err) console.error('Error: scouting report update')
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('scoutingReport')
        res.body.scoutingReport.should.have.property('matchID').that.equals('59dbab61e971f74d21b48759')
        res.body.scoutingReport.should.have.property('robotPos').that.equals('blue1')
        done()
      })
  })

  it('Test update scouting report after permission granted', (done) => {
    chai.request(server)
      .post('/api/team/member')
      .set('Authorization', user.token)
      .send({
        email: user2.email
      })
      .end((err, res) => {
        if (err) console.error('Error: team update')
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('team')
        done()
      })
  })

  it('Test update scouting report after permission granted', (done) => {
    chai.request(server)
      .post('/api/team/perm')
      .set('Authorization', user.token)
      .send({
        user: user,
        memberid: user2._id,
        canScout: true,
        canAnalyze: true
      })
      .end((err, res) => {
        if (err) console.error('Error: team update')
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('team')
        done()
      })
  })

  it('Test update scouting report but not admin', (done) => {
    chai.request(server)
      .post('/api/report/scouting/' + scoutingReport._id)
      .set('Authorization', user2.token)
      .send({
        matchID: '59dbab61e971f74d21b48759',
        robotPos: 'red1'
      })
      .end((err, res) => {
        if (!err) console.error('Error: scouting report but not admin')
        res.should.have.status(401)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(false)
        done()
      })
  })

  it('Test delete scouting report', (done) => {
    chai.request(server)
      .delete('/api/report/scouting/' + scoutingReport._id)
      .set('Authorization', user.token)
      .end((err, res) => {
        if (err) console.error('Error: scouting report delete')
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        done()
      })
  })

  it('Test delete team', (done) => {
    chai.request(server)
      .delete('/api/team/')
      .set('Authorization', user.token)
      .end((err, res) => {
        if (err) console.error('Error: team delete')
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        done()
      })
  })

  it('Test user deletion', (done) => {
    chai.request(server)
      .delete('/api/account')
      .set('content-type', 'application/x-www-form-urlencoded')
      .set('Authorization', user.token)
      .send({
        password: user.password
      })
      .end((err, res) => {
        if (err) console.error('Error: account delete')
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        done()
      })
  })

  it('Test user deletion', (done) => {
    chai.request(server)
      .delete('/api/account')
      .set('content-type', 'application/x-www-form-urlencoded')
      .set('Authorization', user2.token)
      .send({
        password: user2.password
      })
      .end((err, res) => {
        if (err) console.error('Error: account delete')
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        done()
      })
  })
})

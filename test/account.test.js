const chai = require('chai')
const chaiHttp = require('chai-http')
const server = require('../index')

let should = chai.should() // eslint-disable-line
chai.use(chaiHttp)

describe('Testing user account', () => {
  let user = {
    email: 'test@gmail.com',
    password: 'testtest',
    firstName: 'testf',
    lastName: 'testl',
    handle: 'test',
    educationLvl: 'College',
    Country: 'USA',
    stateOrProv: 'Indiana',
    city: 'West Lafayette',
    school: 'Purdue University'
  }

  it('Test user registration', (done) => {
    chai.request(server)
      .post('/api/auth/register')
      .set('content-type', 'application/x-www-form-urlencoded')
      .send(user)
      .end((err, res) => {
        if (err) console.error('Error: user registration')
        res.should.have.status(201)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('token')
        res.body.should.have.property('user')
        user._id = res.body.user._id
        user.token = res.body.token
        done()
      })
  })

  it('Test duplicate user registration', (done) => {
    chai.request(server)
      .post('/api/auth/register')
      .set('content-type', 'application/x-www-form-urlencoded')
      .send(user)
      .end((err, res) => {
        if (err) {}
        res.should.have.status(400)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(false)
        res.body.should.have.property('error').that.equals('Email address already in use!')
        done()
      })
  })

  it('Test user login', (done) => {
    chai.request(server)
      .post('/api/auth/login')
      .set('content-type', 'application/x-www-form-urlencoded')
      .send({
        email: user.email,
        password: user.password
      })
      .end((err, res) => {
        if (err) console.error('Error: user login')
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('token')
        res.body.should.have.property('user')
        user.token = res.body.token
        done()
      })
  })

  it('Test user token refresh', (done) => {
    chai.request(server)
      .get('/api/auth/token')
      .set('content-type', 'application/x-www-form-urlencoded')
      .set('Authorization', user.token)
      .send()
      .end((err, res) => {
        if (err) console.error('Error: user token refresh')
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('token')
        user.token = res.body.token
        done()
      })
  })

  it('Test user get account', (done) => {
    chai.request(server)
      .get('/api/account')
      .set('Authorization', user.token)
      .send()
      .end((err, res) => {
        if (err) console.error('Error: user get account')
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('user')
        done()
      })
  })

  it('Test user change password', (done) => {
    chai.request(server)
      .post('/api/account/password')
      .set('Authorization', user.token)
      .send({
        oldPassword: user.password,
        newPassword: 'testtesttest'
      })
      .end((err, res) => {
        if (err) console.error('Error: user change password')
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('user')
        done()
      })
  })

  it('Test user deletion', (done) => {
    chai.request(server)
      .delete('/api/account')
      .set('content-type', 'application/x-www-form-urlencoded')
      .set('Authorization', user.token)
      .send({
        password: user.password
      })
      .end((err, res) => {
        if (err) console.error('Error: user deletion')
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        done()
      })
  })
})

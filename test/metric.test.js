const chai = require('chai')
const expect = chai.expect
const metricController = require('../controllers/metric')

describe('Testing Metrics', () => {
  let metric = {
    name: 'test',
    section: 'Autonomous Mode',
    description: 'testest',
    type: 'Integer',
    defaultValue: '1',
    maximumValue: 10,
    minimumValue: 1,
    incrementStep: 1
  }

  let metric2 = {
    name: 'New Name',
    section: 'Autonomous Mode',
    description: 'New Description',
    type: 'Integer',
    defaultValue: '1',
    maximumValue: 10,
    minimumValue: 1,
    incrementStep: 1
  }

  let metric3 = {
    name: 'New Name',
    section: 'Autonomous Mode',
    description: 'New Description',
    type: 'Radio',
    radioOptions: ['1', '2', '3'],
    defaultValue: '1'
  }

  it('constructs a new instance', (done) => {
    let req = {body: metric}
    let newMetric = metricController.createNewMetric(req)
    expect(newMetric).to.deep.equal(metric)
    done()
  })

  it('updates correctly', function (done) {
    let oldMetric = metricController.createNewMetric({body: metric})
    let updateObject = {name: metric2.name, description: metric2.description}
    let updatedMetric = metricController.updateMetric({body: updateObject}, oldMetric)
    expect(updatedMetric).to.be.an('object').that.deep.equals(metric2)
    done()
  })

  it('changes type correctly', function (done) {
    let oldMetric = metricController.createNewMetric({body: metric2})
    let updateObject = {type: metric3.type, radioOptions: metric3.radioOptions}
    let updatedMetric = metricController.updateMetric({body: updateObject}, oldMetric)
    expect(updatedMetric).to.deep.equal(metric3)
    done()
  })
})

const chai = require('chai')
const chaiHttp = require('chai-http')
const server = require('../index')

chai.use(chaiHttp)

describe('Testing matches', () => {
  let user = {
    email: 'test@gmail.com',
    password: 'testtest',
    firstName: 'testf',
    lastName: 'testl',
    handle: 'test',
    educationLvl: 'College',
    Country: 'USA',
    stateOrProv: 'Indiana',
    city: 'West Lafayette',
    school: 'Purdue University'
  }

  let match = {
    number: 1,
    finalResult: 42
  }

  it('Test user registration', (done) => {
    chai.request(server)
      .post('/api/auth/register')
      .set('content-type', 'application/x-www-form-urlencoded')
      .send(user)
      .end((err, res) => {
        if (err) console.error('Error: registration')
        res.should.have.status(201)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('token')
        res.body.should.have.property('user')
        user._id = res.body.user._id
        user.token = res.body.token
        done()
      })
  })

  it('Test match creation', (done) => {
    chai.request(server)
      .post('/api/match/')
      .set('content-type', 'application/x-www-form-urlencoded')
      .set('Authorization', user.token)
      .send(match)
      .end((err, res) => {
        if (err) console.error('Error: match creation')
        res.should.have.status(201)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('match')
        res.body.match.should.have.property('number').that.equals(1)
        match._id = res.body.match._id
        done()
      })
  })

  it('Test duplicate match creation', (done) => {
    chai.request(server)
      .post('/api/match/')
      .set('content-type', 'application/x-www-form-urlencoded')
      .set('Authorization', user.token)
      .send(match)
      .end((err, res) => {
        if (err) {}
        res.should.have.status(400)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(false)
        res.body.should.have.property('error').that.equals('Match already exists with same title!')
        done()
      })
  })

  it('Test get match', (done) => {
    chai.request(server)
      .get('/api/match/' + match._id)
      .set('Authorization', user.token)
      .end((err, res) => {
        if (err) console.error('Error: match get')
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('match')
        res.body.match.should.have.property('number').that.equals(1)
        done()
      })
  })

  it('Test update match', (done) => {
    chai.request(server)
      .post('/api/match/' + match._id)
      .set('Authorization', user.token)
      .send({
        number: 2
      })
      .end((err, res) => {
        if (err) console.error('Error: match update')
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('match')
        res.body.match.should.have.property('number').that.equals(2)
        done()
      })
  })

  it('Test delete match', (done) => {
    chai.request(server)
      .delete('/api/match/' + match._id)
      .set('Authorization', user.token)
      .end((err, res) => {
        if (err) console.error('Error: match deletion')
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        done()
      })
  })

  it('Test user deletion', (done) => {
    chai.request(server)
      .delete('/api/account')
      .set('content-type', 'application/x-www-form-urlencoded')
      .set('Authorization', user.token)
      .send({
        password: user.password
      })
      .end((err, res) => {
        if (err) console.error('Error: user delete')
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        done()
      })
  })
})

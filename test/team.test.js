const chai = require('chai')
const chaiHttp = require('chai-http')
const server = require('../index')

chai.use(chaiHttp)

describe('Testing teams', () => {
  let user = {
    email: 'test@gmail.com',
    password: 'testtest',
    firstName: 'testf',
    lastName: 'testl',
    handle: 'test',
    educationLvl: 'College',
    Country: 'USA',
    stateOrProv: 'Indiana',
    city: 'West Lafayette',
    school: 'Purdue University',
    canScout: true,
    canAnalyze: true
  }

  let user2 = {
    email: 'test2@gmail.com',
    password: 'testtesttest',
    firstName: 'testf2',
    lastName: 'testl2',
    handle: 'test2',
    educationLvl: 'College',
    Country: 'USA',
    stateOrProv: 'Indiana',
    city: 'West Lafayette',
    school: 'Purdue University',
    canScout: false,
    canAnalyze: false
  }

  let team = {
    name: 'test',
    number: 5,
    info: {
      address: {
        school: 'purdue'
      }
    }
  }

  let event = {
    name: 'test',
    location: 'Indiana'
  }

  let game = {
    name: 'gameTest',
    description: 'This is a test'
  }

  it('Test user registration', (done) => {
    chai.request(server)
      .post('/api/auth/register')
      .set('content-type', 'application/x-www-form-urlencoded')
      .send(user)
      .end((err, res) => {
        if (err) console.error('Error: user register')
        res.should.have.status(201)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('token')
        res.body.should.have.property('user')
        user._id = res.body.user._id
        user.token = res.body.token
        done()
      })
  })

  it('Test second user registration', (done) => {
    chai.request(server)
      .post('/api/auth/register')
      .set('content-type', 'application/x-www-form-urlencoded')
      .send(user2)
      .end((err, res) => {
        if (err) console.error('Error: user register')
        res.should.have.status(201)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('token')
        res.body.should.have.property('user')
        user2._id = res.body.user._id
        user2.token = res.body.token
        done()
      })
  })

  it('Test get team by Id with an invalid Team Id', (done) => {
    chai.request(server)
        .get('/api/team/' + user._id)
        .set('Authorization', user.token)
        .end((err, res) => {
          if (!err) console.error('Error: unsuccessful team get')
          res.should.have.status(400)
          res.body.should.be.a('object')
          res.body.should.have.property('success').that.equals(false)
          res.body.should.have.property('error').that.equals('That team does not exist!')
          done()
        })
  })

  it('Test team creation', (done) => {
    chai.request(server)
      .post('/api/team/')
      .set('Authorization', user.token)
      .send(team)
      .end((err, res) => {
        if (err) console.error('Error: team creation')
        res.should.have.status(201)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('team')
        res.body.team.should.have.property('name').that.equals('test')
        res.body.team.should.have.property('info')
        res.body.team.info.should.have.property('address')
        res.body.team.info.address.should.have.property('school').that.equals('purdue')
        team._id = res.body.team._id
        done()
      })
  })

  it('Test duplicate team creation', (done) => {
    chai.request(server)
        .post('/api/team/')
        .set('Authorization', user2.token)
        .send(team)
        .end((err, res) => {
          if (err) {}
          res.should.have.status(400)
          res.body.should.be.a('object')
          res.body.should.have.property('success').that.equals(false)
          res.body.should.have.property('error').that.equals('Team already exists with the same name!')
          done()
        })
  })

  it('Test successful get team by Id', (done) => {
    chai.request(server)
      .get('/api/team/' + team._id)
      .set('Authorization', user.token)
      .end((err, res) => {
        if (err) console.error('Error: team get')
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('team')
        res.body.team.should.have.property('name').that.equals('test')
        done()
      })
  })

  it('Test successful get current team', (done) => {
    chai.request(server)
        .get('/api/team/')
        .set('Authorization', user.token)
        .end((err, res) => {
          if (err) console.error('Error: team get')
          res.should.have.status(200)
          res.body.should.be.a('object')
          res.body.should.have.property('success').that.equals(true)
          res.body.should.have.property('team')
          res.body.team.should.have.property('name').that.equals('test')
          done()
        })
  })

  it('Test unsuccessful get current team', (done) => {
    chai.request(server)
        .get('/api/team/')
        .set('Authorization', user2.token)
        .end((err, res) => {
          if (!err) console.error('Error: unsuccessful team get')
          res.should.have.status(422)
          res.body.should.be.a('object')
          res.body.should.have.property('success').that.equals(false)
          res.body.should.have.property('error').that.equals('You do not have a team')
          done()
        })
  })

  it('Test update team', (done) => {
    chai.request(server)
      .post('/api/team/update')
      .set('Authorization', user.token)
      .send({
        number: 6
      })
      .end((err, res) => {
        if (err) console.error('Error: team update')
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('team')
        res.body.team.should.have.property('number').that.equals(6)
        done()
      })
  })

  it('Test add team member', (done) => {
    chai.request(server)
      .post('/api/team/member')
      .set('Authorization', user.token)
      .send({
        email: user2.email
      })
      .end((err, res) => {
        if (err) console.error('Error: team update')
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('team')
        // res.body.team.should.have.property('members').that.equals([1321321,32190312j,231])
        done()
      })
  })

  it('Test set team member permission', (done) => {
    chai.request(server)
      .post('/api/team/perm')
      .set('Authorization', user.token)
      .send({
        user: user,
        memberid: user2._id,
        canScout: true,
        canAnalyze: true
      })
      .end((err, res) => {
        if (err) console.error('Error: team update')
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('team')
        done()
      })
  })

  it('Test view team member permission after updateTeamMemberPermisison', (done) => {
    chai.request(server)
      .get('/api/account')
      .set('Authorization', user2.token)
      .end((err, res) => {
        if (err) console.error('Error: user get account')
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('user')
        res.body.user.should.have.property('canScout').that.equals(true)
        res.body.user.should.have.property('canAnalyze').that.equals(true)
        done()
      })
  })

  it('Test remove team member', (done) => {
    chai.request(server)
      .post('/api/team/member/delete')
      .set('Authorization', user.token)
        .send({
          user: user,
          memberid: user2._id
        })
      .end((err, res) => {
        if (err) console.error('Error: team update')
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('team')
        done()
      })
  })

  it('Test event creation', (done) => {
    chai.request(server)
      .post('/api/event/')
      .set('content-type', 'application/x-www-form-urlencoded')
      .set('Authorization', user.token)
      .send(event)
      .end((err, res) => {
        if (err) console.error('Error: event creation')
        res.should.have.status(201)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('event')
        res.body.event.should.have.property('name').that.equals(event.name)
        event._id = res.body.event._id
        done()
      })
  })

  it('Test game creation', (done) => {
    chai.request(server)
      .post('/api/games/')
      .set('content-type', 'application/x-www-form-urlencoded')
      .set('Authorization', user.token)
      .send(game)
      .end((err, res) => {
        if (err) console.error('Error: game creation')
        res.should.have.status(201)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('game')
        res.body.game.should.have.property('name').that.equals('gameTest')
        res.body.game.should.have.property('description').that.equals('This is a test')
        game._id = res.body.game._id
        done()
      })
  })

  it('Test delete event', (done) => {
    chai.request(server)
      .delete('/api/event/' + event._id)
      .set('Authorization', user.token)
      .send()
      .end((err, res) => {
        if (err) console.error('Error: event deletion')
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        done()
      })
  })

  it('Test delete team', (done) => {
    chai.request(server)
      .delete('/api/team/')
      .set('Authorization', user.token)
      .end((err, res) => {
        if (err) console.error('Error: team delete')
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        done()
      })
  })

  it('Test user teamid deleted', (done) => {
    chai.request(server)
      .get('/api/account')
      .set('Authorization', user.token)
      .send()
      .end((err, res) => {
        if (err) console.error('Error: user get account')
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        res.body.should.have.property('user')
        res.body.user.should.have.property('teamID').that.equals(null)
        done()
      })
  })

  it('Test delete event', (done) => {
    chai.request(server)
      .delete('/api/event/' + event._id)
      .set('Authorization', user.token)
      .end((err, res) => {
        if (err) console.error('Error: event delete')
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        done()
      })
  })

  it('Test delete game', (done) => {
    chai.request(server)
      .delete('/api/games/' + game._id)
      .set('Authorization', user.token)
      .end((err, res) => {
        if (err) console.error('Error: game deletion')
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        done()
      })
  })

  it('Test user deletion', (done) => {
    chai.request(server)
      .delete('/api/account')
      .set('content-type', 'application/x-www-form-urlencoded')
      .set('Authorization', user.token)
      .send({
        password: user.password
      })
      .end((err, res) => {
        if (err) console.error('Error: user creation')
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        done()
      })
  })

  it('Test second user deletion', (done) => {
    chai.request(server)
      .delete('/api/account')
      .set('content-type', 'application/x-www-form-urlencoded')
      .set('Authorization', user2.token)
      .send({
        password: user2.password
      })
      .end((err, res) => {
        if (err) console.error('Error: user creation')
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('success').that.equals(true)
        done()
      })
  })
})

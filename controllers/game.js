// const util = require('./util')
const Game = require('../models/Game')
const metricController = require('./metric')

exports.getGameById = (req, res, next) => {
  // const userInfo = util.getUserInfo(req.user)
  const gameid = req.params.gameid

  if (!gameid) {
    return res.status(422).json({
      success: false,
      error: 'No id supplied'
    })
  }

  Game.findById(gameid).exec((err, game) => {
    if (err) {
      return res.status(500).json({
        success: false,
        error: err
      })
    }

    if (!game) {
      return res.status(400).json({
        success: false,
        error: 'That game does not exist!'
      })
    }

    return res.json({
      success: true,
      game: game
    })
  })
}

exports.deleteGameById = (req, res, next) => {
  // const userInfo = util.getUserInfo(req.user)
  const gameid = req.params.gameid

  if (!gameid) {
    return res.status(422).json({
      success: false,
      error: 'No id supplied'
    })
  }

  Game.remove({_id: gameid}, (err) => {
    if (err) {
      return res.status(500).json({
        success: false,
        error: err
      })
    }
    return res.json({
      success: true
    })
  })
}

exports.postNewGame = (req, res, next) => {
  // const userInfo = util.getUserInfo(req.user)

  const name = req.body.name
  const description = req.body.description
  const metrics = req.body.metrics

  if (!req.user.teamID) {
    return res.status(400).json({
      success: false,
      error: "You don't have a team."
    })
  }

  let game = new Game({
    name: name,
    description: description,
    metrics: metrics,
    teamID: req.user.teamID
  })

  game.save((err, newGame) => {
    if (err) {
      return res.status(500).json({
        success: false,
        error: err
      })
    }

    return res.status(201).json({
      success: true,
      game: newGame
    })
  })
}

exports.postUpdateGame = (req, res, next) => {
  // const userInfo = util.getUserInfo(req.user)
  const gameid = req.params.gameid

  const name = req.body.name
  const description = req.body.description

  if (!gameid) {
    return res.status(422).json({
      success: false,
      error: 'No id supplied!'
    })
  }

  if (!(description)) {
    return res.status(422).json({
      sucsess: false,
      error: 'No field supplied!'
    })
  }

  Game.findById(gameid, (err, existingGame) => {
    if (err) {
      return res.status(500).json({
        success: false,
        error: err
      })
    }

    if (!existingGame) {
      return res.status(400).json({
        success: false,
        error: 'That game does not exist!'
      })
    }

    if (name) { existingGame.name = name }
    if (description) { existingGame.description = description }

    existingGame.save((err, updatedGame) => {
      if (err) {
        return res.status(500).json({
          success: false,
          error: err
        })
      }

      return res.json({
        success: true,
        game: updatedGame
      })
    })
  })
}

exports.postAddMetricToGame = (req, res, next) => {
  let gameid = req.params.gameid
  let metric = metricController.createNewMetric(req)

  if (!gameid) {
    return res.status(422).json({
      success: false,
      error: 'No id supplied!'
    })
  }

  if (!metric) {
    return res.status(400).json({
      success: false,
      error: 'Invalid Metric'
    })
  }

  Game.findById(gameid, (err, game) => {
    if (err) {
      return res.status(500).json({
        success: false,
        error: err
      })
    }
    if (!game) {
      return res.status(400).json({
        success: false,
        error: 'That game does not exist!'
      })
    }
    game.metrics.push(metric)
    game.save((err, game) => {
      if (err) {
        return res.status(500).json({
          success: false,
          error: err
        })
      }
      res.json({
        success: true,
        metrics: game.metrics
      })
    })
  })
}

exports.updateMetric = (req, res) => {
  Game.findById(req.params.gameid, (err, game) => {
    if (err) {
      return res.status(500).json({
        success: false,
        error: err
      })
    }
    if (!game) {
      return res.status(400).json({
        success: false,
        error: 'That game does not exist!'
      })
    }
    let metric = game.metrics.id(req.params.metricID)

    if (!metric) {
      return res.json({
        success: false,
        error: "That metric doesn't exist."
      })
    }

    metric = metricController.updateMetric(req, metric)

    game.save((err, game) => {
      if (err) {
        return res.status(500).json({
          success: false,
          error: err
        })
      }
      res.json({
        success: true,
        metric: game.metrics.id(req.params.metricID)
      })
    })
  })
}

exports.deleteMetricFromGame = (req, res) => {
  Game.findById(req.params.gameid, (err, game) => {
    if (err) {
      return res.status(500).json({
        success: false,
        error: err
      })
    }

    if (!game) {
      return res.status(400).json({
        success: false,
        error: 'That game does not exist!'
      })
    }
    game.metrics.pull(req.params.metricID)
    game.save(game, (err, game) => {
      if (err) {
        return res.status(500).json({
          success: false,
          error: err
        })
      }
      return res.json({
        success: true
      })
    })
  })
}

exports.getGameMetrics = (req, res) => {
  const gameid = req.params.gameid

  Game.findById(gameid, (err, game) => {
    if (err) {
      return res.status(500).json({
        success: false,
        error: err
      })
    }

    if (!game) {
      return res.status(400).json({
        success: false,
        error: 'That game does not exist!'
      })
    }

    return res.json(game.metrics)
  })
}

exports.getAllGames = (req, res) => {
  if (!req.user.teamID) {
    return res.status(400).json({
      success: false,
      error: "You don't have a team."
    })
  }

  Game.find({teamID: req.user.teamID}, (err, games) => {
    if (err) {
      return res.status(500).json({
        success: false,
        error: err
      })
    }

    return res.json({
      success: true,
      games: games
    })
  })
}

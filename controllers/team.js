const util = require('./util')
const Team = require('../models/Team')
const User = require('../models/User')

exports.getCurrentTeam = (req, res, next) => {
  const userInfo = util.getUserInfo(req.user)
  const teamid = userInfo.teamID

  if (!teamid) {
    return res.status(422).json({
      success: false,
      error: 'You do not have a team'
    })
  }

  Team.findById(teamid)
      .populate('admins', '-password')
      .populate('members', '-password')
      .exec((err, team) => {
        if (err) {
          return res.status(500).json({
            success: false,
            error: err
          })
        }

        if (!team) {
          return res.status(400).json({
            success: false,
            error: 'That team does not exist!'
          })
        }

        return res.json({
          success: true,
          team: team
        })
      })
}

exports.getTeamById = (req, res, next) => {
  const teamid = req.params.teamid

  if (!teamid) {
    return res.status(422).json({
      success: false,
      error: 'No id supplied'
    })
  }

  Team.findById(teamid)
  .populate('admins', '-password')
  .populate('members', '-password')
  .exec((err, team) => {
    if (err) {
      return res.status(500).json({
        success: false,
        error: err
      })
    }

    if (!team) {
      return res.status(400).json({
        success: false,
        error: 'That team does not exist!'
      })
    }

    return res.json({
      success: true,
      team: team
    })
  })
}

exports.deleteTeam = (req, res, next) => {
  const userInfo = util.getUserInfo(req.user)

  Team.findOne({
    _id: userInfo.teamID,
    admins: {
      $in: [userInfo._id]
    }}) // Team with admins contains userid
    .exec((err, existingTeam) => {
      if (err) {
        return res.status(500).json({
          success: false,
          error: err
        })
      }

      if (!existingTeam) {
        return res.status(400).json({
          success: false,
          error: 'Team not found or you do not have permissions to delete it!'
        })
      }

      Team.remove({_id: userInfo.teamID}, (err) => {
        if (err) {
          return res.status(500).json({
            success: false,
            error: err
          })
        }

        User.find({teamID: existingTeam._id}, (err, users) => {
          if (err) {
            console.error('Error on teamScheme remove middleware')
          }

          users.forEach((usr) => {
            usr.teamID = null
            usr.save((err, newUser) => {
              if (err) {
                console.error(err)
              }
            })
          })

          return res.json({
            success: true
          })
        })
      })
    })
}

exports.postNewTeam = (req, res, next) => {
  const userInfo = util.getUserInfo(req.user)

  const name = req.body.name
  const number = req.body.number
  let bio
  let webpage
  let picture
  let country
  let stateOrProv
  let city
  let school
  if (req.body.info) {
    const info = req.body.info
    if (info) {
      if (info.bio) { bio = info.bio }
      if (info.webpage) { webpage = info.webpage }
      if (info.picture) { picture = info.picture }
      if (info.address) {
        if (info.address.country) { country = info.address.country }
        if (info.address.stateOrProv) { stateOrProv = info.address.stateOrProv }
        if (info.address.city) { city = info.address.city }
        if (info.address.school) { school = info.address.school }
      }
    }
  }

  if (!name) {
    return res.status(422).json({
      success: false,
      error: 'No name supplied!'
    })
  }

  Team.findOne({ name: name })
    .populate('admins', '-password')
    .populate('members', '-password')
    .exec((err, existingTeam) => {
      if (err) {
        return res.status(500).json({
          success: false,
          error: err
        })
      }

      if (existingTeam) {
        return res.status(400).json({
          success: false,
          error: 'Team already exists with the same name!'
        })
      }

      const admins = [userInfo._id]
      const members = [userInfo._id]
      const attendedEvents = []

      let team = new Team({
        name: name,
        number: number,
        admins: admins,
        members: members,
        attendedEvents: attendedEvents,
        info: {
          bio: bio,
          webpage: webpage,
          picture: picture,
          address: {
            country: country,
            stateOrProv: stateOrProv,
            city: city,
            school: school
          }
        }
      })

      team.save((err, newTeam) => {
        if (err) {
          return res.status(500).json({
            success: false,
            error: err
          })
        }

        User.findById(userInfo._id, (err, existingUser) => {
          if (err) {
            return res.status(500).json({
              success: false,
              error: err
            })
          }

          existingUser.teamID = newTeam._id
          existingUser.canScout = true
          existingUser.canAnalyze = true
          existingUser.save((err) => {
            if (err) {
              return res.status(500).json({
                success: false,
                error: err
              })
            }

            return res.status(201).json({
              success: true,
              team: newTeam,
              currentUser: existingUser
            })
          })
        })
      })
    })
}

exports.postUpdateTeamInfo = (req, res, next) => {
  const userInfo = util.getUserInfo(req.user)

  const name = req.body.name
  const number = req.body.number
  let bio
  let webpage
  let picture
  let country
  let stateOrProv
  let city
  let school
  if (req.body.info) {
    const info = req.body.info
    if (info) {
      if (info.bio) { bio = info.bio }
      if (info.webpage) { webpage = info.webpage }
      if (info.picture) { picture = info.picture }
      if (info.address) {
        if (info.address.country) { country = info.address.country }
        if (info.address.stateOrProv) { stateOrProv = info.address.stateOrProv }
        if (info.address.city) { city = info.address.city }
        if (info.address.school) { school = info.address.school }
      }
    }
  }

  if (!(name || number || bio || webpage || picture || country || stateOrProv || city || school)) {
    return res.status(422).json({
      success: false,
      error: 'No fields supplied!'
    })
  }

  Team.findOne({
    _id: userInfo.teamID,
    admins: {
      $in: [userInfo._id]
    }}) // Team with admins contains userid
    .populate('admins', '-password')
    .populate('members', '-password')
    .exec((err, existingTeam) => {
      if (err) {
        return res.status(500).json({
          success: false,
          error: err
        })
      }

      if (!existingTeam) {
        return res.status(400).json({
          success: false,
          error: 'Team not found or you do not have permissions to update it!'
        })
      }

      if (name) { existingTeam.name = name }
      if (number) { existingTeam.number = number }
      if (bio) { existingTeam.info.bio = bio }
      if (webpage) { existingTeam.info.webpage = webpage }
      if (picture) { existingTeam.info.picture = picture }
      if (country) { existingTeam.info.address.country = country }
      if (stateOrProv) { existingTeam.info.address.stateOrProv = stateOrProv }
      if (city) { existingTeam.info.address.city = city }
      if (school) { existingTeam.info.address.school = school }

      existingTeam.save((err, newTeam) => {
        if (err) {
          return res.status(500).json({
            success: false,
            error: err
          })
        }

        return res.json({
          success: true,
          team: newTeam
        })
      })
    })
}

exports.deleteTeamMember = (req, res, next) => {
  const userInfo = util.getUserInfo(req.user)

  const memberid = req.body.memberid

  if (!memberid) {
    return res.status(422).json({
      success: false,
      error: 'No member id found'
    })
  }

  Team.findOne({
    _id: userInfo.teamID, // Team with user teamID
    admins: {
      $in: [userInfo._id], // admins contains userid
      $nin: [memberid] // admins does not contain memberid
    },
    members: {
      $in: [memberid]
    }}) // members contains memberid
    .populate('admins', '-password')
    .populate('members', '-password')
    .exec((err, existingTeam) => {
      if (err) {
        return res.status(500).json({
          success: false,
          error: err
        })
      }

      if (!existingTeam) {
        return res.status(400).json({
          success: false,
          error: 'Permission error!'
        })
      }

      existingTeam.members.pull(memberid)

      existingTeam.save((err, newTeam) => {
        if (err) {
          return res.status(500).json({
            success: false,
            error: err
          })
        }

        User.findById(memberid, (err, user) => {
          if (err) {
            return res.status(500).json({
              success: false,
              error: err
            })
          }

          user.teamID = null

          user.save((err, user) => {
            if (err) {
              return res.status(500).json({
                success: false,
                error: err
              })
            }

            return res.json({
              success: true,
              team: newTeam
            })
          })
        })
      })
    })
}

exports.postTeamMember = (req, res, next) => {
  const userInfo = util.getUserInfo(req.user)

  const email = req.body.email

  if (!email) {
    return res.status(422).json({
      success: false,
      error: 'No email found'
    })
  }
  User.findOne({email: email}, (err, existingUser) => {
    if (err) {
      return res.status(500).json({
        success: false,
        error: err
      })
    }

    if (!existingUser) {
      return res.status(422).json({
        success: false,
        error: 'User not found'
      })
    }

    Team.findOne({
      _id: userInfo.teamID,
      admins: {
        $in: [userInfo._id] // Team with admins contains userid
      },
      members: {
        $nin: [existingUser._id] // memberid not already a member
      }})
      .populate('admins', '-password')
      .populate('members', '-password')
      .exec((err, existingTeam) => {
        if (err) {
          return res.status(500).json({
            success: false,
            error: err
          })
        }

        if (!existingTeam) {
          return res.status(400).json({
            success: false,
            error: 'Permission error or team does not exist!'
          })
        }

        User.findById(existingUser._id, (err, user) => {
          if (err) {
            return res.status(500).json({
              success: false,
              error: err
            })
          }
          if (!user) {
            return res.status(400).json({
              success: false,
              error: 'User not found'
            })
          }
          user.teamID = existingTeam._id
          user.save((err, newUser) => {
            if (err) {
              return res.status(500).json({
                success: false,
                error: err
              })
            }
            existingTeam.members.push(newUser._id)
            existingTeam.save((err, newTeam) => {
              if (err) {
                return res.status(500).json({
                  success: false,
                  error: err
                })
              }
              return res.json({
                success: true,
                team: newTeam
              })
            })
          })
        })
      })
  })
}

exports.postUpdateTeamMemberPermission = (req, res, next) => {
  const userInfo = util.getUserInfo(req.user)

  const memberid = req.body.memberid
  const canScout = req.body.canScout
  const canAnalyze = req.body.canAnalyze
  const canAdmin = req.body.canAdmin

  Team.findOne({
    _id: userInfo.teamID,
    admins: {
      $in: [userInfo._id], // Team with admins contains userid
      $nin: [memberid] // User with memberid not an admin
    },
    members: {
      $in: [memberid] // User with memberid is an admin
    }})
    .populate('admins', '-password')
    .populate('members', '-password')
    .exec((err, existingTeam) => {
      if (err) {
        return res.status(500).json({
          success: false,
          error: err
        })
      }

      if (!existingTeam) {
        return res.status(400).json({
          success: false,
          error: 'Team does not exist or you do not have permissions'
        })
      }

      User.findById(memberid, (err, user) => {
        if (err) {
          return res.status(500).json({
            success: false,
            error: err
          })
        }

        if (!user) {
          return res.status(400).json({
            success: false,
            error: 'That user does not exist!'
          })
        }

        if (canScout != null) { user.canScout = canScout }
        if (canAnalyze != null) { user.canAnalyze = canAnalyze }

        user.save((err, newUser) => {
          if (err) {
            return res.status(500).json({
              success: false,
              error: err
            })
          }

          if (canAdmin) {
            existingTeam.admins.push(newUser._id)
            existingTeam.save((err, newTeam) => {
              if (err) {
                return res.status(500).json({
                  success: false,
                  error: err
                })
              }

              return res.json({
                success: true,
                team: newTeam
              })
            })
          }
          return res.json({
            success: true,
            team: existingTeam
          })
        })
      })
    })
}

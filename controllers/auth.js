const nodemailer = require('nodemailer')
const async = require('async')
const crypto = require('crypto')
const User = require('../models/User')
const config = require('../config')
const util = require('./util')

exports.postRegister = (req, res, next) => {
  const email = req.body.email
  const password = req.body.password
  const firstName = req.body.firstName
  const lastName = req.body.lastName
  const handle = req.body.handle
  const educationLvl = req.body.educationLvl
  const country = req.body.counry
  const stateOrProv = req.body.stateOrProv
  const city = req.body.city
  const school = req.body.school
  const profilePic = req.body.profilePic

  if (!email) {
    return res.status(422).json({
      sucess: false,
      error: 'No email address supplied!'
    })
  }

  if (!firstName || !lastName) {
    return res.status(422).json({
      success: false,
      error: 'No first name or last name supplied!'
    })
  }

  if (!password) {
    return res.status(422).json({
      success: false,
      error: 'No password supplied!'
    })
  }

  User.findOne({ email: email }, (err, existingUser) => {
    if (err) {
      return res.status(500).json({
        success: false,
        error: err
      })
    }

    if (existingUser) {
      return res.status(400).json({
        success: false,
        error: 'Email address already in use!'
      })
    }

    let user = new User({
      email: email,
      password: password,
      profile: {
        firstName: firstName,
        lastName: lastName,
        userHandle: handle,
        educationLvl: educationLvl,
        address: {
          country: country,
          stateOrProv: stateOrProv,
          city: city,
          school: school
        }
      },
      canScout: false,
      canAnalyze: false,
      profilePic: profilePic
    })

    user.save((err, user) => {
      if (err) {
        return res.status(500).json({
          success: false,
          error: err
        })
      }

      const userInfo = util.getUserInfo(user)

      return res.status(201).json({
        success: true,
        token: 'Bearer ' + util.generateToken(userInfo),
        user: userInfo
      })
    })
  })
}

exports.postLogin = (req, res, next) => {
  const userInfo = util.getUserInfo(req.user)

  return res.json({
    success: true,
    token: 'Bearer ' + util.generateToken(userInfo),
    user: userInfo
  })
}

exports.getToken = (req, res, next) => {
  const userInfo = util.getUserInfo(req.user)

  return res.json({
    success: true,
    token: 'Bearer ' + util.generateToken(userInfo)
  })
}

exports.postForgot = (req, res, next) => {
  async.waterfall([
    (done) => {
      crypto.randomBytes(20, (err, buf) => {
        let token = buf.toString('hex')
        done(err, token)
      })
    },
    (token, done) => {
      User.findOne({ email: req.body.email }, (err, user) => {
        if (!user || err) {
          console.log('error', 'No account with that email address exists.')
          return
        }
        user.resetPasswordToken = token
        user.resetPasswordExpires = Date.now() + 3600000 // 1 hour

        user.save((err) => {
          done(err, token, user)
        })
      })
    },
    (token, user, done) => {
      console.log(config)
      let smtpTrans = nodemailer.createTransport({
        service: 'Gmail',
        auth: {
          user: config.email,
          pass: config.emailPassword
        }
      })
      let mailOptions = {
        to: user.email,
        from: 'myemail',
        subject: 'Friss Password Reset',
        text: 'You are receiving this because you have requested the reset of the password for your account.\n\n' +
          'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
          'http://' + req.headers.host + '/reset/' + token + '\n\n' +
          'If you did not request this, please ignore this email and your password will remain unchanged.\n'
      }
      smtpTrans.sendMail(mailOptions, (err) => {
        if (err) console.error(err)
      })
    }
  ], (err) => {
    console.log('emailing error' + ' ' + err)
  })
}

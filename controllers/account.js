const util = require('./util')
const User = require('../models/User')
const Team = require('../models/Team')

exports.getAccount = (req, res, next) => {
  const userInfo = util.getUserInfo(req.user)

  res.json({
    success: true,
    user: userInfo
  })
}

exports.deleteAccount = (req, res, next) => {
  const userInfo = util.getUserInfo(req.user)

  const password = req.body.password

  if (!password) {
    return res.status(422).json({
      success: false,
      error: 'No password supplied!'
    })
  }

  User.remove({_id: userInfo._id}, (err) => {
    if (err) {
      return res.status(400).json({
        success: false,
        error: 'That user does not exist!'
      })
    }

    if (!userInfo.teamID) {
      return res.json({
        success: true
      })
    }

    Team.findById(userInfo.teamID, (err, existingTeam) => {
      if (err) {
        return res.status(400).json({
          success: false,
          error: err
        })
      }

      if (!existingTeam) {
        return res.json({
          success: true
        })
      }

      existingTeam.admins.remove(userInfo._id)
      existingTeam.members.remove(userInfo._id)

      existingTeam.save((err, newTeam) => {
        if (err) {
          return res.status(400).json({
            success: false,
            error: err
          })
        }
        return res.json({
          success: true
        })
      })
    })
  })
}

exports.postUpdateProfile = (req, res, next) => {
  const userInfo = util.getUserInfo(req.user)

  const firstName = req.body.firstName
  const lastName = req.body.lastName
  const userHandle = req.body.userHandle
  const educationLvl = req.body.educationLvl
  const country = req.body.country
  const stateOrProv = req.body.stateOrProv
  const city = req.body.city
  const school = req.body.school
  const teamID = req.body.teamID
  const canScout = req.body.canScout
  const canAnalyze = req.body.canAnalyze
  const profilePic = req.body.profilePic

  if (!(firstName || lastName || userHandle || educationLvl ||
      country || stateOrProv || city || school || teamID ||
      canScout || canAnalyze || profilePic)) {
    return res.status(422).json({
      sucess: false,
      error: 'No field supplied!'
    })
  }

  User.findOne({ email: userInfo.email }, (err, existingUser) => {
    if (err) {
      return res.status(500).json({
        success: false,
        error: err
      })
    }

    if (!existingUser) {
      return res.status(400).json({
        success: false,
        error: 'That user does not exist!'
      })
    }

    if (firstName) { existingUser.profile.firstName = firstName }
    if (lastName) { existingUser.profile.lastName = lastName }
    if (userHandle) { existingUser.profile.userHandle = userHandle }
    if (educationLvl) { existingUser.profile.educationLvl = educationLvl }
    if (country) { existingUser.profile.address.country = country }
    if (stateOrProv) { existingUser.profile.address.stateOrProv = stateOrProv }
    if (city) { existingUser.profile.address.city = city }
    if (school) { existingUser.profile.address.school = school }
    if (teamID) { existingUser.teamID = teamID }
    if (canScout) { existingUser.canScout = canScout }
    if (canAnalyze) { existingUser.canAnalyze = canAnalyze }
    if (profilePic) { existingUser.profilePic = profilePic }

    existingUser.save((err, user) => {
      if (err) {
        return res.status(500).json({
          success: false,
          error: err
        })
      }

      const userInfo = util.getUserInfo(user)
      return res.json({
        success: true,
        user: userInfo
      })
    })
  })
}

exports.postUpdatePassword = (req, res, next) => {
  const userInfo = util.getUserInfo(req.user)

  const newPassword = req.body.newPassword
  const oldPassword = req.body.oldPassword

  if (!oldPassword && !newPassword) {
    return res.status(422).json({
      success: false,
      error: 'No password supplied!'
    })
  }

  User.findById(userInfo._id, (err, existingUser) => {
    if (err) {
      return res.status(500).json({
        success: false,
        error: err
      })
    }

    if (!existingUser) {
      return res.status(400).json({
        success: false,
        error: 'That user does not exist!'
      })
    }

    existingUser.password = newPassword

    existingUser.save((err, user) => {
      if (err) {
        return res.status(500).json({
          success: false,
          error: err
        })
      }

      const userInfo = util.getUserInfo(user)
      res.json({
        success: true,
        user: userInfo
      })
    })
  })
}

const jwt = require('jsonwebtoken')
const config = require('../config')

exports.generateToken = (user) => {
  return jwt.sign(user, config.secret, {
    expiresIn: 60 * 60 * 24 * 7 // seconds = 1 week
  })
}

exports.getUserInfo = (request) => {
  return {
    _id: request._id,
    email: request.email,
    firstName: request.profile.firstName,
    lastName: request.profile.lastName,
    userHandle: request.profile.userHandle,
    educationLvl: request.profile.educationLvl,
    country: request.profile.address.country,
    stateOrProv: request.profile.address.stateOrProv,
    city: request.profile.address.city,
    school: request.profile.address.school,
    teamID: request.teamID,
    canScout: request.canScout,
    canAnalyze: request.canAnalyze,
    profilePic: request.profilePic
  }
}

const async = require('async')
const util = require('./util')
const AllianceReport = require('../models/AllianceReport')
const ScoutingReport = require('../models/ScoutingReport')
const Team = require('../models/Team')
const Event = require('../models/Event')
const Game = require('../models/Game')

exports.getAllianceReportById = (req, res, next) => {
  const userInfo = util.getUserInfo(req.user)
  const reportid = req.params.reportid

  if (!userInfo.canAnalyze && !userInfo.canScout) {
    return res.status(401).json({
      success: false,
      error: 'You do not have the permission to view this!'
    })
  }

  if (!reportid) {
    return res.status(422).json({
      success: false,
      error: 'No id supplied'
    })
  }

  AllianceReport.findById(reportid, (err, allianceReport) => {
    if (err) {
      return res.status(500).json({
        success: false,
        error: err
      })
    }

    if (!allianceReport) {
      return res.status(400).json({
        success: false,
        error: 'That report does not exist!'
      })
    }

    if (!allianceReport.teamID.equals(userInfo.teamID)) {
      return res.status(401).json({
        success: false,
        error: 'That report is for another team!'
      })
    }

    return res.json({
      success: true,
      allianceReport: allianceReport
    })
  })
}

exports.deleteAllianceReportById = (req, res, next) => {
  const userInfo = util.getUserInfo(req.user)
  const reportid = req.params.reportid

  if (!userInfo.canScout) {
    return res.status(401).json({
      success: false,
      error: 'You do not have the permission to delete this!'
    })
  }

  if (!reportid) {
    return res.status(422).json({
      success: false,
      error: 'No id supplied'
    })
  }

  AllianceReport.findById(reportid, (err, allianceReport) => {
    if (err) {
      return res.status(500).json({
        success: false,
        error: err
      })
    }

    if (!allianceReport) {
      return res.status(400).json({
        success: false,
        error: 'That report does not exist!'
      })
    }

    if (!allianceReport.teamID.equals(userInfo.teamID)) {
      return res.status(401).json({
        success: false,
        error: 'That report is for another team!'
      })
    }

    AllianceReport.remove({_id: reportid}, (err) => {
      if (err) {
        return res.status(500).json({
          success: false,
          error: err
        })
      }

      return res.json({
        success: true
      })
    })
  })
}

exports.getAllianceReports = (req, res, next) => {
  const userInfo = util.getUserInfo(req.user)
  if (!userInfo.canScout) {
    return res.status(400).json({
      success: false,
      error: "You don't have a permission!."
    })
  }

  AllianceReport.find({teamID: userInfo.teamID}, (err, reports) => {
    if (err) {
      return res.status(500).json({
        success: false,
        error: err
      })
    }

    return res.json({
      success: true,
      reports: reports
    })
  })
}

function calculateScore (report, weights, game, callback) {
  let totalScore = 0
  for (let i = 0; i < game.metrics.length; i++) {
    let eachMetricScore = 0
    if (game.metrics.id(weights[i].metric).type !== 'Radio') { // metric is int, double, or time
      // .find method not working to find corresponding metric id in report
      for (let j = 0; j < report.metricData.length; j++) {
        if (game.metrics.id(report.metricData[j].metric).equals(game.metrics.id(weights[i].metric))) { // simplify this
          // console.log('weight: ' + weights[i].weight + '  report val:  ' + report.metricData[j].metricValue)
          eachMetricScore = weights[i].weight * report.metricData[j].metricValue
        }
      }
    } else { // metric is radio or bool or string
      // not implemented yet
    }
    totalScore = totalScore + eachMetricScore
  }
  return callback(null, totalScore)
}

exports.postNewAllianceReport = (req, res, next) => {
  const userInfo = util.getUserInfo(req.user)
  if (!userInfo.canAnalyze) {
    return res.status(401).json({
      success: false,
      error: 'You do not have the permission to create this!'
    })
  }

  const name = req.body.name
  const description = req.body.description
  const allianceList = req.body.allianceList
  const submittedBy = userInfo._id
  const teamID = userInfo.teamID
  const eventID = req.body.eventID
  const weights = req.body.weights
  // const teamKey = req.body.teamKey

  if (!teamID) {
    return res.status(401).json({
      success: false,
      error: 'Please enter a team id'
    })
  }
  // make sure weights are valid here?

  // find all scouting reports by team id
  var allResults = []
  ScoutingReport.find({teamID: teamID}, (err, reports) => {
    if (err) {
      return res.status(500).json({
        success: false,
        error: err
      })
    }
    // find all unique scout teams
    let reportSet = new Set()
    reports.forEach((rprt) => {
      reportSet.add(rprt.teamKey)
    })

    // go through each scout team and apply weights
    async.forEachOf(reportSet, (item, key, cb) => {
      let runningScore = 0
      let amountReports = 0
      let averageScore = 0
      ScoutingReport.find({eventID: eventID, teamID: teamID, teamKey: item}, (err, allReports) => {
        // calculate score based on weights
        if (err) {
          return res.status(500).json({
            success: false,
            error: err
          })
        }

        // find game to correspond to the report
        Event.findById(eventID, (err, event) => {
        // calculate score based on weight for each team's robot
          if (err) {
            return res.status(500).json({
              success: false,
              error: err
            })
          }
          Game.findById(event.game, (err, gameList) => {
            if (err) {
              return res.status(500).json({
                success: false,
                error: err
              })
            }

            for (let eachReport of allReports) {
              calculateScore(eachReport, weights, gameList, (err, result) => {
                if (err) {
                  return res.status(500).json({
                    success: false,
                    error: err
                  })
                }
                runningScore = runningScore + result
                amountReports = amountReports + 1
              })
            }
            averageScore = runningScore / amountReports
            let newResult = {
              teamNum: item,
              score: averageScore
            }
            allResults.push(newResult)
            cb()
          })
        })
      })
    }, err => {
      if (err) console.log(err.message)
      allResults.sort((a, b) => {
        return a.score - b.score
      })
      let allianceReport = new AllianceReport({
        name: name,
        description: description,
        allianceList: allianceList,
        submittedBy: submittedBy,
        teamID: teamID,
        weights: weights,
        results: allResults
      })

      allianceReport.save((err, newAllianceReport) => {
        if (err) {
          return res.status(500).json({
            success: false,
            error: err
          })
        }

        return res.status(201).json({
          success: true,
          allianceReport: newAllianceReport
        })
      })
    })
  })
}

exports.postModifyWeightAllianceReport = (req, res, next) => {
  const userInfo = util.getUserInfo(req.user)
  let reportid = req.params.reportid

  if (!userInfo.canAnalyze) {
    return res.status(401).json({
      success: false,
      error: 'You do not have the permission to update this!'
    })
  }

  const metric = req.body.metric
  const weight = req.body.weight

  if (!reportid) {
    return res.status(422).json({
      success: false,
      error: 'No id supplied'
    })
  }

  if (!(metric || weight)) {
    return res.status(422).json({
      success: false,
      error: 'No metric or weight provided'
    })
  }

  AllianceReport.findById(reportid, (err, allianceReport) => {
    if (err) {
      return res.status(500).json({
        success: false,
        error: err
      })
    }

    if (!allianceReport) {
      return res.status(400).json({
        success: false,
        error: 'That report does not exist!'
      })
    }

    if (!allianceReport.teamID.equals(userInfo.teamID)) {
      return res.status(401).json({
        success: false,
        error: 'You do not have permission to view this!'
      })
    }

/*
    allianceReport.update({'weights.metric': metric},
        {'$set': { 'weights.$.weight': weight }},
        (err, allianceReport) => {
      if (err) {
        console.log(err)
        return res.status(500).json({
          success: false,
          error: err
        })
      }
      return res.status(200).json({
        success: true,
        allianceReport: allianceReport
      })
    })
*/

    // I couldn't quite figure out how to make the above do the following
    // Feel free to replace this code with the above if you can get it to work
    for (let i = 0; i < allianceReport.weights.length; i++) {
      if (allianceReport.weights[i].metric.equals(metric)) {
        allianceReport.weights[i].weight = weight
        break
      }
    }

    allianceReport.save((err, newReport) => {
      if (err) {
        return res.status(500).json({
          success: false,
          error: err
        })
      }

      return res.json({
        success: true,
        allianceReport: newReport
      })
    })
  })
}

exports.postUpdateAllianceReport = (req, res, next) => {
  const userInfo = util.getUserInfo(req.user)
  let reportid = req.params.reportid

  if (!userInfo.canAnalyze) {
    return res.status(401).json({
      success: false,
      error: 'You do not have the permission to update this!'
    })
  }

  const name = req.body.name
  const allianceList = req.body.allianceList
  const weight = req.body.weight

  if (!reportid) {
    return res.status(422).json({
      success: false,
      error: 'No id supplied'
    })
  }

  if (!(name || allianceList || weight)) {
    return res.status(422).json({
      success: false,
      error: 'No field supplied!'
    })
  }

  AllianceReport.findById(reportid, (err, allianceReport) => {
    if (err) {
      return res.status(500).json({
        success: false,
        error: err
      })
    }

    if (!allianceReport) {
      return res.status(400).json({
        success: false,
        error: 'That report does not exist!'
      })
    }

    if (!allianceReport.teamID.equals(userInfo.teamID)) {
      return res.status(401).json({
        success: false,
        error: 'You do not have permission to view this!'
      })
    }

    if (name) { allianceReport.name = name }
    if (allianceList) { allianceReport.allianceList = allianceList }
    if (weight) {
      if (allianceReport.weights) {
        allianceReport.weights.push(weight)
      } else {
        allianceReport.weights = [weight]
      }
    }

    allianceReport.save((err, newReport) => {
      if (err) {
        return res.status(500).json({
          success: false,
          error: err
        })
      }

      return res.json({
        success: true,
        allianceReport: newReport
      })
    })
  })
}

exports.getScoutingReportById = (req, res, next) => {
  const userInfo = util.getUserInfo(req.user)

  if (!userInfo.canAnalyze && !userInfo.canScout) {
    return res.status(401).json({
      success: false,
      error: 'You do not have permission to view this!'
    })
  }

  const reportid = req.params.reportid

  if (!reportid) {
    return res.status(422).json({
      success: false,
      error: 'No id supplied'
    })
  }

  ScoutingReport.findById(reportid, (err, scoutingReport) => {
    if (err) {
      return res.status(500).json({
        success: false,
        error: err
      })
    }

    if (!scoutingReport) {
      return res.status(400).json({
        success: false,
        error: 'That report does not exist!'
      })
    }

    if (!scoutingReport.teamID.equals(userInfo.teamID)) {
      return res.status(401).json({
        success: false,
        error: 'You do not have permission to view this!'
      })
    }

    return res.json({
      success: true,
      scoutingReport: scoutingReport
    })
  })
}

exports.deleteScoutingReportById = (req, res, next) => {
  const userInfo = util.getUserInfo(req.user)
  const reportid = req.params.reportid

  if (!userInfo.canScout) {
    return res.status(401).json({
      success: false,
      error: 'You do not have permission to delete this!'
    })
  }

  if (!reportid) {
    return res.status(422).json({
      success: false,
      error: 'No id supplied'
    })
  }

  ScoutingReport.findById(reportid, (err, scoutingReport) => {
    if (err) {
      return res.status(500).json({
        success: false,
        error: err
      })
    }

    if (!scoutingReport) {
      return res.status(400).json({
        success: false,
        error: 'That report does not exist!'
      })
    }

    if (!scoutingReport.teamID.equals(userInfo.teamID)) {
      return res.status(401).json({
        success: false,
        error: 'That report is for another team!'
      })
    }

    ScoutingReport.remove({_id: reportid}, (err) => {
      if (err) {
        return res.status(500).json({
          success: false,
          error: err
        })
      }

      return res.json({
        success: true
      })
    })
  })
}

exports.postNewScoutingReport = (req, res, next) => {
  const userInfo = util.getUserInfo(req.user)

  if (!userInfo.canScout) {
    return res.status(401).json({
      success: false,
      error: 'You do not have permission to do that'
    })
  }

  const matchID = req.body.matchID
  const matchNumber = req.body.matchNumber
  const eventID = req.body.eventID
  const metricData = req.body.metricData
  const teamKey = req.body.teamKey
  const robotPos = req.body.robotPos
  const submittedBy = req.user._id
  const teamID = userInfo.teamID

  if (!(matchID || eventID || metricData || submittedBy || robotPos || teamKey)) {
    return res.status(422).json({
      success: false,
      error: 'Missing a field!'
    })
  }

  ScoutingReport.findOne(
    {
      matchID: matchID,
      matchNumber: matchNumber,
      eventID: eventID,
      submittedBy: submittedBy,
      robotPos: robotPos
    }, (err, existingScoutingReport) => {
    if (err) {
      return res.status(500).json({
        success: false,
        error: err
      })
    }

    if (existingScoutingReport) {
      return res.status(400).json({
        success: false,
        error: 'Scouting Report already exists!'
      })
    }

    let scoutingReport = new ScoutingReport({
      matchID: matchID,
      teamKey: teamKey,
      matchNumber: matchNumber,
      eventID: eventID,
      metricData: metricData,
      submittedBy: submittedBy,
      teamID: teamID,
      robotPos: robotPos
    })

    scoutingReport.save((err, newScoutingReport) => {
      if (err) {
        return res.status(500).json({
          success: false,
          error: err
        })
      }

      return res.status(201).json({
        success: true,
        scoutingReport: newScoutingReport
      })
    })
  })
}

exports.postUpdateScoutingReport = (req, res, next) => {
  const userInfo = util.getUserInfo(req.user)
  const reportid = req.params.reportid

  if (!userInfo.canScout) {
    return res.status(401).json({
      success: false,
      error: 'You do not have permission!'
    })
  }

  const matchID = req.body.matchID
  const eventID = req.body.eventID
  const metrics = req.body.metrics
  const robotPos = req.body.robotPos
  const scoutTeam = req.body.scoutTeam

  if (!reportid) {
    return res.status(422).json({
      success: false,
      error: 'No id supplied'
    })
  }

  if (!(matchID || eventID || metrics || robotPos)) {
    return res.status(422).json({
      sucess: false,
      error: 'No field supplied!'
    })
  }

  ScoutingReport.findById(reportid, (err, existingReport) => {
    if (err) {
      return res.status(500).json({
        success: false,
        error: err
      })
    }

    if (!existingReport) {
      return res.status(400).json({
        success: false,
        error: 'That report does not exist!'
      })
    }

    Team.findOne({
      _id: userInfo.teamID,
      admins: {
        $in: [userInfo._id]
      }}) // Team with admins contains userid
      .exec((err, existingTeam) => {
        if (err) {
          return res.status(500).json({
            success: false,
            error: err
          })
        }

        if (!existingTeam) {
          return res.status(401).json({
            success: false,
            error: 'You do not have permission!'
          })
        }

        if (matchID) { existingReport.matchID = matchID }
        if (eventID) { existingReport.eventID = eventID }
        if (metrics) { existingReport.metrics = metrics }
        if (robotPos) { existingReport.robotPos = robotPos }
        if (scoutTeam) { existingReport.scoutTeam = scoutTeam }

        existingReport.save((err, newReport) => {
          if (err) {
            return res.status(500).json({
              success: false,
              error: err
            })
          }

          return res.json({
            success: true,
            scoutingReport: newReport
          })
        })
      })
  })
}

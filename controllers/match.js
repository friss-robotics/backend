// const util = require('./util')
const Match = require('../models/Match.js')

let matchToJson = (match) => {
  return {
    _id: match._id,
    number: match.number,
    alliance: match.alliance,
    finalResult: match.finalResult
  }
}

exports.getMatchById = (req, res, next) => {
  // const userInfo = util.getUserInfo(req.user)
  const matchid = req.params.matchid

  if (!matchid) {
    return res.status(422).json({
      success: false,
      error: 'No id supplied'
    })
  }

  Match.findById(matchid, (err, match) => {
    if (err) {
      return res.status(500).json({
        success: false,
        error: err
      })
    }

    if (!match) {
      return res.status(400).json({
        success: false,
        error: 'That match does not exist!'
      })
    }

    const matchJson = matchToJson(match)

    return res.json({
      success: true,
      match: matchJson
    })
  })
}

exports.deleteMatchById = (req, res, next) => {
  // const userInfo = util.getUserInfo(req.user)
  const matchid = req.params.matchid
  if (!matchid) {
    return res.status(422).json({
      sucess: false,
      error: 'No id supplied'
    })
  }

  Match.remove({_id: matchid}, (err) => {
    if (err) {
      return res.status(500).json({
        success: false,
        error: err
      })
    }

    return res.json({
      success: true
    })
  })
}

exports.postNewMatch = (req, res, next) => {
  // const userInfo = util.getUserInfo(req.user)

  const number = req.body.number
  const alliance = req.body.alliance
  const finalResult = req.body.finalResult

  if (!number) {
    return res.status(422).json({
      sucess: false,
      error: 'No number supplied!'
    })
  }

  Match.findOne({ number: number }, (err, existingMatch) => {
    if (err) {
      return res.status(500).json({
        success: false,
        error: err
      })
    }

    if (existingMatch) {
      return res.status(400).json({
        success: false,
        error: 'Match already exists with same title!'
      })
    }

    let match = new Match({
      number: number,
      alliance: alliance,
      finalResult: finalResult
    })

    match.save((err, newMatch) => {
      if (err) {
        return res.status(500).json({
          success: false,
          error: err
        })
      }

      let matchInfo = matchToJson(newMatch)
      return res.status(201).json({
        success: true,
        match: matchInfo
      })
    })
  })
}

exports.postUpdateMatch = (req, res, next) => {
  // const userInfo = util.getUserInfo(req.user)
  const matchid = req.params.matchid

  const number = req.body.number
  const alliance = req.body.alliance
  const finalResult = req.body.finalResult

  if (!matchid) {
    return res.status(422).json({
      success: false,
      error: 'No id supplied!'
    })
  }

  if (!(number || alliance || finalResult)) {
    return res.status(422).json({
      sucess: false,
      error: 'No field supplied!'
    })
  }

  Match.findById(matchid, (err, existingMatch) => {
    if (err) {
      return res.status(500).json({
        success: false,
        error: err
      })
    }

    if (!existingMatch) {
      return res.status(400).json({
        success: false,
        error: 'That match does not exist!'
      })
    }

    if (number) { existingMatch.number = number }
    if (alliance) { existingMatch.alliance = alliance }
    if (finalResult) { existingMatch.finalResult = finalResult }

    existingMatch.save((err, match) => {
      if (err) {
        return res.status(500).json({
          success: false,
          error: err
        })
      }

      return res.json({
        success: true,
        match: match
      })
    })
  })
}

// const util = require('./util')
const Event = require('../models/Event')
const ScoutingReport = require('../models/ScoutingReport')

exports.getTeamEvents = (req, res) => {
  if (!req.user.teamID) {
    return res.status(400).json({
      success: false,
      error: "You don't have a team."
    })
  }

  Event.find({team: req.user.teamID}, (err, events) => {
    if (err) {
      return res.status(500).json({
        success: false,
        error: err
      })
    }
    return res.json({
      success: true,
      events
    })
  })
}

exports.getEventScoutingReports = (req, res) => {
  const eventid = req.params.eventid

  if (!eventid) {
    return res.status(422).json({
      sucess: false,
      error: 'No id supplied'
    })
  }

  ScoutingReport.find({eventID: eventid}, (err, reports) => {
    if (err) {
      return res.status(500).json({
        success: false,
        error: err
      })
    }
    res.json({success: true, reports: reports})
  })
}

exports.getEventById = (req, res, next) => {
  // const userInfo = util.getUserInfo(req.user)
  const eventid = req.params.eventid

  if (!eventid) {
    return res.status(422).json({
      sucess: false,
      error: 'No id supplied'
    })
  }

  Event.findById(eventid, (err, event) => {
    if (err) {
      return res.status(500).json({
        success: false,
        error: err
      })
    }

    if (!event) {
      return res.status(400).json({
        success: false,
        error: 'That event does not exist!'
      })
    }

    return res.json({
      success: true,
      event: event
    })
  })
}

exports.deleteEventById = (req, res, next) => {
  // const userInfo = util.getUserInfo(req.user)
  const eventid = req.params.eventid

  if (!eventid) {
    return res.status(422).json({
      success: false,
      error: 'No eventid supplied!'
    })
  }

  Event.remove({ _id: eventid }, (err) => {
    if (err) {
      return res.status(500).json({
        success: false,
        error: err
      })
    }

    return res.json({
      success: true
    })
  })
}

exports.postNewEvent = (req, res, next) => {
  // const userInfo = util.getUserInfo(req.user)

  const name = req.body.name
  const date = req.body.date
  const location = req.body.location
  const team = req.user.teamID
  const attendingTeams = req.body.attendingTeams
  const allianceReports = req.body.allianceReports
  const scoutingReports = req.body.scoutingReports
  const matches = req.body.matches
  const game = req.body.game

  if (!team) {
    return res.status(400).json({
      success: false,
      error: 'You are not on a team.'
    })
  }

  let evnt = new Event({
    name: name,
    date: date,
    location: location,
    eventKey: req.body.eventKey,
    team: team,
    attendingTeams: attendingTeams,
    allianceReports,
    scoutingReports,
    matches: matches,
    game: game
  })

  evnt.save((err, newEvent) => {
    if (err) {
      return res.status(500).json({
        success: false,
        error: err
      })
    }

    return res.status(201).json({
      success: true,
      event: newEvent
    })
  })
}

exports.postUpdateEvent = (req, res, next) => {
  // const userInfo = util.getUserInfo(req.user)
  const eventid = req.params.eventid

  const name = req.body.name
  const date = req.body.date
  const location = req.body.location
  const attendingTeams = req.body.attendingTeams
  const scoutingReports = req.body.scoutingReports
  const matches = req.body.matches
  const game = req.body.game

  if (!eventid) {
    return res.status(422).json({
      success: false,
      error: 'No id supplied!'
    })
  }

  if (!(name || date || location || attendingTeams ||
      scoutingReports || matches || game)) {
    return res.status(422).json({
      sucess: false,
      error: 'No field supplied!'
    })
  }

  Event.findById(eventid, (err, existingEvent) => {
    if (err) {
      return res.status(500).json({
        success: false,
        error: err
      })
    }

    if (!existingEvent) {
      return res.status(400).json({
        success: false,
        error: 'That event does not exist!'
      })
    }

    if (name) { existingEvent.name = name }
    if (date) { existingEvent.date = date }
    if (location) { existingEvent.location = location }
    if (attendingTeams) { existingEvent.attendingTeams = attendingTeams }
    if (scoutingReports) { existingEvent.scoutingReports = scoutingReports }
    if (matches) { existingEvent.matches = matches }
    if (game) { existingEvent.game = game }

    existingEvent.save((err, event) => {
      if (err) {
        return res.status(500).json({
          success: false,
          error: err
        })
      }

      return res.json({
        success: true,
        event: event
      })
    })
  })
}

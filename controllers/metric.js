// creates a new metric from a metric creation request object
exports.createNewMetric = (req) => {
  const name = req.body.name
  const description = req.body.description
  const section = req.body.section
  const type = req.body.type
  let defaultValue
  let maximumValue
  let minimumValue
  let incrementStep
  let radioOptions
  if (type === 'Integer' || type === 'Double') {
    defaultValue = req.body.defaultValue
    maximumValue = req.body.maximumValue
    minimumValue = req.body.minimumValue
    incrementStep = req.body.incrementStep
  }
  if (type === 'Time') {
    defaultValue = req.body.defaultValue
    maximumValue = req.body.maximumValue
  }
  if (type === 'Radio') {
    radioOptions = req.body.radioOptions
  }

  let metric
  if (type === 'Integer' || type === 'Double') {
    metric = {
      name: name,
      description: description,
      section: section,
      type: type,
      defaultValue: defaultValue,
      maximumValue: maximumValue,
      minimumValue: minimumValue,
      incrementStep: incrementStep
    }
  }

  if (type === 'Time') {
    metric = {
      name: name,
      description: description,
      section: section,
      type: type,
      defaultValue: defaultValue,
      maximumValue: maximumValue
    }
  }

  if (type === 'Radio') {
    metric = {
      name: name,
      description: description,
      section: section,
      type: type,
      radioOptions: radioOptions
    }
  }

  if (type === 'String') {
    metric = {
      name: name,
      description: description,
      section: section,
      type: type,
      defaultValue: defaultValue
    }
  }

  return metric
}
// takes a metric update request and an existing metric and returns the updated metric
// todo: it should only update fields that are included in the update request. Irrelevant fields should be removed?
exports.updateMetric = (req, existingMetric) => {
  const name = req.body.name
  const description = req.body.description
  const type = req.body.type
  const section = req.body.section

  let defaultValue
  let maximumValue
  let minimumValue
  let incrementStep
  let radioOptions

  if (type === 'Integer' || type === 'Double') {
    defaultValue = req.body.defaultValue
    maximumValue = req.body.maximumValue
    minimumValue = req.body.minimumValue
    incrementStep = req.body.incrementStep
  }
  if (type === 'Time') {
    defaultValue = req.body.defaultValue
    maximumValue = req.body.maximumValue
  }
  if (type === 'Radio') {
    radioOptions = req.body.radioOptions
    delete existingMetric.minimumValue
    delete existingMetric.maximumValue
    delete existingMetric.incrementStep
  }
  if (type === 'String') {
    defaultValue = req.body.defaultValue
  }

  if (!(name || description || type || section || defaultValue || maximumValue || minimumValue || incrementStep || radioOptions)) {
    return null
  }

  if (!existingMetric) {
    return null
  }

  if (name) { existingMetric.name = name }
  if (description) { existingMetric.description = description }
  if (type) { existingMetric.type = type }
  if (section) { existingMetric.section = section }
  if (defaultValue) { existingMetric.defaultValue = defaultValue }
  if (maximumValue) { existingMetric.maximumValue = maximumValue }
  if (minimumValue) { existingMetric.minimumValue = minimumValue }
  if (incrementStep) { existingMetric.incrementStep = incrementStep }
  if (radioOptions) { existingMetric.radioOptions = radioOptions }

  return existingMetric
}

const express = require('express')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const mongoose = require('mongoose')
const config = require('./config')
const router = require('./routes')

mongoose.Promise = global.Promise
mongoose.connect(config.database, { useMongoClient: true }, (err) => {
  if (err) {
    console.log('Failed to connect to database:' + config.database)
    process.exit(1)
  }
  
})

const app = express()

if (process.env.TESTING !== 'true') { app.use(morgan('dev')) }

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

router.setupRoutes(app)

app.listen(config.port, () => {
  console.log('Server running on port ' + config.port)
})

module.exports = app

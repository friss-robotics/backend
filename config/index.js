const dotenv = require('dotenv')
const path = require('path')
if (process.env.NODE_ENV === 'prod') {
  dotenv.config({path: path.join(__dirname, '../.env')})
} else {
  dotenv.config({path: path.join(__dirname, '../.env.test')})
}

module.exports = {
  'secret': process.env.JWT_SECRET,
  'database': process.env.DATABASE_URI,
  'port': process.env.PORT,
  'blue': process.env.BLUE_ALLIANCE_KEY,
  'email': process.env.EMAIL,
  'emailPassword': process.env.EMAIL_PASSWORD
}

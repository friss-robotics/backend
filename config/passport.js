const passport = require('passport')
const User = require('../models/User')
const config = require('./index')
const JwtStrategy = require('passport-jwt').Strategy
const ExtractJwt = require('passport-jwt').ExtractJwt
const LocalStrategy = require('passport-local')

const localOptions = { usernameField: 'email' }

const localLogin = new LocalStrategy(localOptions, (email, password, done) => {
  User.findOne({ email: email }, (err, user) => {
    if (err) return done(err)
    if (!user) {
      return done(null, false, {
        error: 'Your login details are incorrect! '
      })
    }

    user.comparePassword(password, (err, isMatch) => {
      if (err) return done(err)
      if (!isMatch) {
        return done(null, false, {
          error: 'Your login details are incorrect! '
        })
      }
      return done(null, user)
    })
  })
})

const jwtOptions = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: config.secret
}

const jwtLogin = new JwtStrategy(jwtOptions, (payload, done) => {
  User.findById(payload._id, (err, user) => {
    if (err) return done(err, false)
    if (!user) {
      return done(null, false, {
        error: 'Your login details are incorrect! '
      })
    }
    if (user) return done(null, user)
  })
})

passport.use(localLogin)
passport.use(jwtLogin)

exports.checkAuth = passport.authenticate('jwt', { session: false })
exports.checkLogin = passport.authenticate('local', { session: false })

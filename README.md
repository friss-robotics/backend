## FRISS Backend
## Install:
```
nodejs
npm/yarn
```

### To Run:
```
git clone https://gitlab.com/friss-robotics/backend.git
cd backend
npm install --save
npm start
```


## API
|Endpoint   |Type   |Description   |
|---|---|---|
|**/api/auth**|||
|/api/auth/register|post|Register an account|
|/api/auth/login|post|Logs in an account|
|/api/auth/forgot|post|Forgot password|
|**/api/account**|||
|/api/account/|get|Gets account info|
|/api/account/|delete|Deletes an account|
|/api/account/profile|post|Updates an account|
|/api/account/passord|post|Changes account password|
|**/api/team**|||
|/api/team/|get|Gets current team information of user|
|/api/team/:teamid|get|Gets team info by id|
|/api/team/:teamid|delete|Deletes a team by id|
|/api/team/|post|Creates a new team|
|**/api/report**|||
|*/api/report/alliance*|||
|/api/report/alliance/:reportid|get|Gets alliance report by id|
|/api/report/alliance/:reportid|delete|Deletes an alliance report by id|
|/api/report/alliance/|post|Creates a new alliance report|
|*/api/report/scouting*|||
|/api/report/scouting/:reportid|get|Gets scouting report by id|
|/api/report/scouting/:reportid|delete|Deletes a scouting report by id|
|/api/report/scouting/|post|Creates a new scouting report|
|**/api/metric**|||
|/api/metric/:metricid|get|Gets a metric by id|
|/api/metric/:metricid|delete|Deletes a metric by id|
|/api/metric/|post|Creates a new metric|
|**/api/match**|||
|/api/match/:matchid|get|Gets a match by id|
|/api/match/:matchid|delete|Deletes a match by id|
|/api/match/|post|Creates a new match|
|**/api/event**|||
|/api/event/:eventid|get|Gets an event by id|
|/api/event/:eventid|delete|Deletes an event by id|
|/api/event/|post|Creates a new event|

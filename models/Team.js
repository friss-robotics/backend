const mongoose = require('mongoose')
const ObjectId = mongoose.Schema.ObjectId

const teamSchema = new mongoose.Schema({
  name: String,
  number: Number,
  admins: [{type: ObjectId, ref: 'User'}],
  members: [{type: ObjectId, ref: 'User'}],
  info: {
    bio: String,
    webpage: String,
    picture: String,
    address: {
      country: String,
      stateOrProv: String,
      city: String,
      school: String
    }
  }
}, {timestamps: true})

module.exports = mongoose.model('Team', teamSchema)

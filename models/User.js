const mongoose = require('mongoose')
const bcrypt = require('bcrypt')
const ObjectId = mongoose.Schema.ObjectId

const userSchema = new mongoose.Schema({
  email: { type: String, unique: true },
  password: String,
  profile: {
    firstName: String,
    lastName: String,
    userHandle: String,
    educationLvl: String,
    address: {
      country: String,
      stateOrProv: String,
      city: String,
      school: String
    }
  },
  teamID: ObjectId,
  canScout: Boolean,
  canAnalyze: Boolean,
  profilePic: String,
  passwordResetToken: String,
  passwordResetExpires: Date
}, { timestamps: true })

userSchema.pre('save', function (next) {
  const user = this

  if (!user.isModified('password')) { return next() }

  bcrypt.genSalt(10, function (err, salt) {
    if (err) return next(err)

    bcrypt.hash(user.password, salt, function (err, hash) {
      if (err) return next(err)
      user.password = hash
      next()
    })
  })
})

userSchema.methods.comparePassword = function (candidatePassword, cb) {
  bcrypt.compare(candidatePassword, this.password, function (err, isMatch) {
    if (err) return cb(err)
    cb(err, isMatch)
  })
}

module.exports = mongoose.model('User', userSchema)

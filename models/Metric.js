const mongoose = require('mongoose')

const metricSchema = new mongoose.Schema({
  name: String,
  section: String, // UI will list metrics by section
  description: String,
  type: String, // integer, double, time, checkbox, radio(dropdown)
  defaultValue: String, // int, dub, time, checkbox(set to T or F),
  maximumValue: Number, // int, dub, time
  minimumValue: Number, // int, dub,
  incrementStep: Number, // int, dub, determines numeric increment/decrement step for frontend UI
  radioOptions: [String] // radio, for dropdown or radio button metrics

}, {timestamps: true})

module.exports = metricSchema

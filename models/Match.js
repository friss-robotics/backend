const mongoose = require('mongoose')
const ObjectId = mongoose.Schema.ObjectId

const matchSchema = new mongoose.Schema({
  number: Number,
  alliance: ObjectId,
  finalResult: Number
}, {timestamps: true})

module.exports = mongoose.model('Match', matchSchema)

const mongoose = require('mongoose')
const ObjectId = mongoose.Schema.ObjectId

const scoutingReportSchema = new mongoose.Schema({
  matchID: String, // the Blue Alliance Match ID (optional?)
  teamKey: String, // The key of the team being scouted
  matchNumber: Number,
  eventID: ObjectId,
  metricData: [{
    metric: ObjectId,
    metricValue: String
  }],
  submittedBy: ObjectId,
  teamID: ObjectId, // the ID of the team that the report belongs to
  robotPos: {
    type: String,
    enum: ['red1', 'red2', 'red3', 'blue1', 'blue2', 'blue3']
  }
}, {timestamps: true})

module.exports = mongoose.model('ScoutingReport', scoutingReportSchema)

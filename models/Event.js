const mongoose = require('mongoose')
const ObjectId = mongoose.Schema.ObjectId

const eventSchema = new mongoose.Schema({
  name: String,
  date: Date,
  location: String,
  eventKey: String,
  team: ObjectId, // the team that created and "owns" the event
  attendingTeams: [ObjectId], // team _ids - unnecessary?
  allianceReports: [ObjectId], // aliance report _id's
  scoutingReports: [ObjectId], // scounting report _id's
  game: ObjectId  // Game _id's
}, {timestamps: true})

module.exports = mongoose.model('Event', eventSchema)

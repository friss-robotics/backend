const mongoose = require('mongoose')
const ObjectId = mongoose.Schema.ObjectId

const allianceReportSchema = new mongoose.Schema({
  name: String,
  description: String,
  allianceList: [ObjectId], // teams _id's
  submittedBy: ObjectId,
  teamID: ObjectId,
  eventID: ObjectId,
  weights: [{
    metric: ObjectId, // what metric it is
    weight: Number // weight = 0 means user didn't select this metric, weight > 0 means metric with high value is good, weight < 0 means metric with high value is bad
  }],
  results: [{
    teamNum: Number,
    score: Number
  }]
}, {timestamps: true})

module.exports = mongoose.model('AllianceReport', allianceReportSchema)

const mongoose = require('mongoose')
const metricSchema = require('./Metric')
const ObjectId = mongoose.Schema.ObjectId

const gameSchema = new mongoose.Schema({
  name: String,
  description: String,
  metrics: [metricSchema],
  teamID: ObjectId
}, {timestamps: true})

module.exports = mongoose.model('Game', gameSchema)

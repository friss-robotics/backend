const express = require('express')
const passportConfig = require('../config/passport')
const AuthController = require('../controllers/auth')

const authRoutes = express.Router()

// /api/auth

// postRegister will create and register a user
// Input: User Information (Email, name, password, etc.)
// Output: A user object and token
authRoutes.post('/register', AuthController.postRegister)

// postLogin will log in as a particular user
// Input: Email, password
// Output: A user object and token
authRoutes.post('/login', passportConfig.checkLogin, AuthController.postLogin)

// getToken will generate a token for a user
// Input: Token
// Output: A token
authRoutes.get('/token', passportConfig.checkAuth, AuthController.getToken)

// postForgot will send an email to a user's email address with a link to reset their password
// Input: Token, User account email
// Output: N/A (Unless an error occurred)
authRoutes.post('/forgot', AuthController.postForgot)

module.exports = authRoutes

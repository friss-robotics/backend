const express = require('express')
const passportConfig = require('../config/passport')
const ReportController = require('../controllers/report')

const reportRoutes = express.Router()

// /api/report

// /api/report/alliance

// getAllianceReportById will retrieve information about a particular Alliance Report
// Input: Token, alliance Report ID
// Output: AllianceReport Object
reportRoutes.get('/alliance/:reportid', passportConfig.checkAuth, ReportController.getAllianceReportById)

// deleteAllianceReportById will delete a particular Alliance Report
// Input: Token, alliance Report ID
// Output: Success of true / false
reportRoutes.delete('/alliance/:reportid', passportConfig.checkAuth, ReportController.deleteAllianceReportById)

// postNewAllianceReport will create a new Alliance Report
// Input: Token, alliance Report Information (Name, Description, Alliance List, Submitted By, Team ID)
// Output: Alliance Report Object
reportRoutes.post('/alliance/', passportConfig.checkAuth, ReportController.postNewAllianceReport)

// postUpdateAllianceReport will update the information of a particular Alliance Report
// Input: Token, alliance Report ID and Any Updated Alliance Report Information (Name, Description, Alliance List, Submitted By, Team ID, Weight)
// Output: Updated Alliance Report Object
reportRoutes.post('/alliance/:reportid', passportConfig.checkAuth, ReportController.postUpdateAllianceReport)

// postModifyWeightAllianceReport will modify the weight for the corresponding metrics
// Input: reportid, metric id, and weight value
// Output: updated allaince report object
reportRoutes.post('/alliance/weight/:reportid', passportConfig.checkAuth, ReportController.postModifyWeightAllianceReport)

// getAllianceReports will get alliance reports for a team
// Input: Token
// Output: AllianceReports
reportRoutes.get('/alliance', passportConfig.checkAuth, ReportController.getAllianceReports)

// /api/scouting

// getScoutingReportById will retrieve information about a particular Scouting Report
// Input: Token, scouting Report ID
// Output: Scouting Report Object
reportRoutes.get('/scouting/:reportid', passportConfig.checkAuth, ReportController.getScoutingReportById)

// deleteScoutingReportById will delete a particular Scouting Report
// Input: Token, scouting Report ID
// Output: Success of true / false
reportRoutes.delete('/scouting/:reportid', passportConfig.checkAuth, ReportController.deleteScoutingReportById)

// postNewScoutingReport will create a new Scouting Report
// Input: Token, scouting Report Information (Match ID, Event ID, Metrics, Robot Position, Submitted By, Team ID)
// Output: Scouting Report Object
reportRoutes.post('/scouting/', passportConfig.checkAuth, ReportController.postNewScoutingReport)

// postUpdateScoutingReport will update the information of a particular Scouting Report
// Input: Token, scouting Report ID and Any Updated Scouting Report Information (Match ID, Event ID, Metrics, Robot Position, Submitted By, Team ID)
// Output: Updated Scouting Report Object
reportRoutes.post('/scouting/:reportid', passportConfig.checkAuth, ReportController.postUpdateScoutingReport)

module.exports = reportRoutes

const express = require('express')
const passportConfig = require('../config/passport')
const AccountController = require('../controllers/account')

const accountRoutes = express.Router()

// /api/account

// getAccount will return user's information
// Input: Token
// Output: Current user profile
accountRoutes.get('/', passportConfig.checkAuth, AccountController.getAccount)

// deleteAccount will delete user's account. If user is on a team, the team's member list will be updated
// Input: Token, password
// Output: success of true or false
accountRoutes.delete('/', passportConfig.checkAuth, AccountController.deleteAccount)

// postUpdateProfile will update user's account.
// Input: Token, Any of the user's information that needs updating (except password)
// Output: Current user profile
accountRoutes.post('/profile', passportConfig.checkAuth, AccountController.postUpdateProfile)

// postUpdatePassword will update user's password.
// Input: Token, old password, new password
// Output: Current user profile
accountRoutes.post('/password', passportConfig.checkAuth, AccountController.postUpdatePassword)

module.exports = accountRoutes

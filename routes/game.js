const express = require('express')
const passportConfig = require('../config/passport')
const GameController = require('../controllers/game')

const gameRoutes = express.Router()

// all of these routes require authentication
gameRoutes.use(passportConfig.checkAuth)

// retrieves all of a user's games
gameRoutes.get('/', GameController.getAllGames)

// postNewGame will create a new game
// Input: Token, game Information (Name, description, metrics)
// Output: Game Object
gameRoutes.post('/', GameController.postNewGame)

// getGameById will retrieve information about a particular game
// Input: Token, game ID
// Output: Game Object
gameRoutes.get('/:gameid', GameController.getGameById)

// postUpdateGame will update the information of a particular Game
// Input: Token, game ID and Any Updated Game Information (Name, description, metrics)
// Output: Updated Game Object
gameRoutes.post('/:gameid', GameController.postUpdateGame)

// deleteGameById will delete a particular game
// Input: Token, game ID
// Output: Success of true / false
gameRoutes.delete('/:gameid', GameController.deleteGameById)

// gets all the metrics for a game
gameRoutes.get('/:gameid/metrics', GameController.getGameMetrics)

// postAddMetricToGame will add a metric to a game
// Input: Token, game id
// Output: Updated Metrics array
gameRoutes.post('/:gameid/metrics', GameController.postAddMetricToGame)

// updates a metric
// Output: the updated Metric
gameRoutes.put('/:gameid/metrics/:metricID', GameController.updateMetric)

// deletes a single metric from a game
// Output: success: true/false
gameRoutes.delete('/:gameid/metrics/:metricID', GameController.deleteMetricFromGame)

module.exports = gameRoutes

const express = require('express')
const authRoutes = require('./auth')
const accountRoutes = require('./account')
const teamRoutes = require('./team')
const reportRoutes = require('./report')
const matchRoutes = require('./match')
const eventRoutes = require('./event')
const gameRoutes = require('./game')

exports.setupRoutes = (app) => {
  const apiRoutes = express.Router()

  // /api routes
  apiRoutes.use('/auth', authRoutes)
  apiRoutes.use('/account', accountRoutes)
  apiRoutes.use('/team', teamRoutes)
  apiRoutes.use('/report', reportRoutes)
  apiRoutes.use('/match', matchRoutes)
  apiRoutes.use('/event', eventRoutes)
  apiRoutes.use('/games', gameRoutes)

  app.use('/api', apiRoutes)
}

const express = require('express')
const passportConfig = require('../config/passport')
const TeamController = require('../controllers/team')

const teamRoutes = express.Router()

// /api/team

// getCurrentTeam will retrieve information about a User's current team
// Input: Token
// Output: Team Object
teamRoutes.get('/', passportConfig.checkAuth, TeamController.getCurrentTeam)

// getTeamById will retrieve information about a particular Team
// Input: Token, Team ID
// Output: Team Object
teamRoutes.get('/:teamid', passportConfig.checkAuth, TeamController.getTeamById)

// deleteTeam will delete a particular Team
// Input: Token (Needs admin)
// Output: Success of true / false
teamRoutes.delete('/', passportConfig.checkAuth, TeamController.deleteTeam)

// postNewTeam will create a new Team
// Input: Token, team information (Name, Number, Bio, Webpage, Picture, Country, etc.)
// Output: Team Object (With calling User being made an admin)
teamRoutes.post('/', passportConfig.checkAuth, TeamController.postNewTeam)

// postUpdateTeam will update the information of a particular Team
// Input: Token (Needs admin), Any Updated Team Information (Number, Bio, Webpage, Picture, Country, etc.)
// Output: Updated Team Object
teamRoutes.post('/update', passportConfig.checkAuth, TeamController.postUpdateTeamInfo)

// deleteTeamMember will remove a particular User from a Team
// Input: Token (Needs admin), Member User ID (The member being removed)
// Output: Updated Team Object
teamRoutes.post('/member/delete', passportConfig.checkAuth, TeamController.deleteTeamMember)

// postTeamMember will add a particular User from a Team
// Input: Token (Needs admin) Member User ID (The user being added)
// Output: Updated Team Object
teamRoutes.post('/member', passportConfig.checkAuth, TeamController.postTeamMember)

// postUpdateTeamMemberPermission will update a particular Member's Permissions
// Input: Token (Needs admin), Member User ID (The member with permissions being changed), Updated Permissions (canScout, canAnalyze, canAdmin)
// Output: Updated Team Object
teamRoutes.post('/perm', passportConfig.checkAuth, TeamController.postUpdateTeamMemberPermission)

module.exports = teamRoutes

const express = require('express')
const passportConfig = require('../config/passport')
const EventController = require('../controllers/event')

const eventRoutes = express.Router()

// /api/event

eventRoutes.get('/', passportConfig.checkAuth, EventController.getTeamEvents)

// getEventById will retrieve information about a particular event
// Input: Token, Event ID
// Output: Event Object
eventRoutes.get('/:eventid', passportConfig.checkAuth, EventController.getEventById)

eventRoutes.get('/:eventid/scoutingReports', passportConfig.checkAuth, EventController.getEventScoutingReports)

// deleteEventById will delete a particular event
// Input: Token, Event ID
// Output: Success of true / false
eventRoutes.delete('/:eventid', passportConfig.checkAuth, EventController.deleteEventById)

// postNewEvent will create a new event
// Input: Token, Event Information (Name, date, location, etc.)
// Output: Event Object
eventRoutes.post('/', passportConfig.checkAuth, EventController.postNewEvent)

// postUpdateEvent will update the information of a particular event
// Input: Token, Event ID and Any Updated Event Information (Name, date, location, etc)
// Output: Updated Event Object
eventRoutes.post('/:eventid', passportConfig.checkAuth, EventController.postUpdateEvent)

module.exports = eventRoutes

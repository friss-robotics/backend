const express = require('express')
const passportConfig = require('../config/passport')
const MatchController = require('../controllers/match')

const matchRoutes = express.Router()

// /api/match

// getMatchById will retrieve information about a particular Match
// Input: Token, match ID
// Output: Match Object
matchRoutes.get('/:matchid', passportConfig.checkAuth, MatchController.getMatchById)

// deleteMatchById will delete a particular Match
// Input: Token, match ID
// Output: Success of true / false
matchRoutes.delete('/:matchid', passportConfig.checkAuth, MatchController.deleteMatchById)

// postNewMatch will create a new Match
// Input: Token, match Information (Number, Alliance, Final Result)
// Output: Match Object
matchRoutes.post('/', passportConfig.checkAuth, MatchController.postNewMatch)

// postUpdateMatch will update the information of a particular Match
// Input: Token, match ID and Any Updated Match Information (Number, Alliance, Final Result)
// Output: Updated Match Object
matchRoutes.post('/:matchid', passportConfig.checkAuth, MatchController.postUpdateMatch)

module.exports = matchRoutes
